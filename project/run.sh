#! /bin/bash

echo 'Now running all commands associated with project'
echo 'For information on modularization or lack of, see README'
echo 'If any errors occur see README'

echo 'This run file does not include making the datasest and queries, and assumes that stage is already done. See README for more.'

echo 'preparation stage, all at index time'
python ./src/testing/tfidf_prep.py
python ./src/testing/make_centroid.py


FILE=array.p
if [ -f "$FILE" ]; then
    echo "$FILE exists. It will not be remade."
else 
    echo 'The file pca.py is being run'
	echo 'This file could take a long time to run'
	python ./src/testing/pca.py
fi




echo 'Boolean methods'
python ./src/testing/processQueries.py Qtex.p and1.run or1.run
python ./src/testing/processQueries.py Q2tex.p and2.run or2.run
python ./src/testing/processQueries.py Q3tex.p and3.run or3.run

echo 'Lucene'
python ./src/testing/IndexFiles.py ./src/testing/tex.p
python ./src/testing/taskfour.py

echo 'tf-idf original'
python ./src/testing/do_tfidf.py
python ./src/testing/do_tfidf.py 2
python ./src/testing/do_tfidf.py 3

echo 'K means clustering and tf-idf cluster 0'
python ./src/testing/basic_clustering.py

echo 'tf-idf with K means clustering parts'
echo 'this method will create many .run files, and will take time to run'
python ./src/testing/tfkm.py

echo 'regular tf-idf and K means tf-idf query expansion'
python ./src/testing/queryexpand.py

echo 'resetting preparation stage, all at index time'
python ./src/testing/tfidf_prep.py
python ./src/testing/make_centroid.py

echo 'PCA clustering'
python ./src/testing/PCA_clustering.py

echo 'PCA as features'
echo 'this method runs slow and will take a moment'
python ./src/testing/pca_features.py

echo 'PCA as features with term frequencies'
python ./src/testing/tfpca.py

echo 'PCA query expansion'
python ./src/testing/qexpandWpca.py
