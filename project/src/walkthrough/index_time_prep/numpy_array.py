import numpy as np
import pickle
from sklearn.decomposition import PCA

# This code, as is, was not directly used, but instead inserted into the function qexpandWpca.loadupfeature

array = pickle.load(open("array.p", "rb"))
array = np.array(array).reshape(408,408)

# doing PCA stuff
pca = PCA(n_components=100)
model = pca.fit_transform(array)
print(len(model))
print(len(model[0]))
print(model[0])
print(model[360])
