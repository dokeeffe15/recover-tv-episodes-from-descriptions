

# This file will create a term frequency compatiable inverted index in multi parts to be read by Team India's tf-idf functions

import pickle

from trec_car.read_data import *
import argparse
import Stemmer

import sys


import math

import India_tokenize

#print("starting")


#AllWords = pickle.load( open( "saveQ.p", "rb" ) )
sum_episodes = pickle.load(open("tex.p","rb"))
#AllWords = pickle.load( open( "saveP.p", "rb" ) )

id_episodes = pickle.load(open("id.p","rb"))


dict = {}
tf = {}
id2id = []
docfreq = {}

cosnorm = []

maxtf = []

# can also do and store  u norm is the same manner as cosine norm


stemmer = Stemmer.Stemmer('english')

stopword = []

#print("37")

# The following lines of code simply retokenize the lines in the paragraphs file
# This was before India_tokenize.py was created and implemened the tokenizer seperately

# load the stop words
stopword = India_tokenize.getstopword()

cw = 0
#print(45)

nstopword = []

for text in stopword:
        if (len(stemmer.stemWord(text)) > 2):
            nstopword.append(stemmer.stemWord(text))

#print("stop words loaded in")





# The following opens the paragraphs cbor file passed as a command line argument

#thearg = sys.argv[1]

#print(thearg)

#args = open(thearg, 'rb')



i = 0
for summary in sum_episodes:
	
	
	
	# every p in iter paragraphs refers to a new paragraph in the cbor

	# the ID of the paragraph is stored in this list, and i becomes the internal document id
	# using id2id[i-1] the relevant paragraph ID can be returned from the internal document id
	i = i + 1
	id2id.append(id_episodes[i-1])
	
	# This list is short hand for: not useful term frequencies. A list created for every paragraph containing the term frequency of every word
	# however, this is not how the term frequency is loaded, its instead related to cosine normalization, and was an afterthought to the original code
	ntf = []
	
	
	querywords = India_tokenize.tokenize_india(summary, stopword)
	
	
	for word in querywords:
		
		if word not in dict:
			dict[word] = []
			tf[word] = {}
			docfreq[word] = 0
			
			# After the word is properly tokenized, its added to dictionary, tf, and docfreq
			# dict is a basic inverted index posting list
			# tf is a double dictionary implemented way to get the term frequency for any word and any document
			# docfreq is a way to store how many documents a given term appears in
			
			dict[word].append(i)
			tf[word][i] = 1
			ntf.append(1)
			docfreq[word] = docfreq[word] + 1

		

		
		else: 
			lastentry = len(dict[word]) - 1
			# If a document and a term already has an entry in the posting list, simple increase the tf rather than making a new entry
			if dict[word][lastentry] == i:
				
				tf[word][i] += 1
				
				le = len(ntf) - 1
				
				
				ntf[le] += 1
				
				
			else:
				dict[word].append(i)
				tf[word][i] = 1
				ntf.append(1)
				docfreq[word] = docfreq[word] + 1
				# If a term already has an entry but doesn't have a document in it, add that document to the right posting lists
			
		
							
						
						
					
					
					
					
	coslen = 0
	mtf = 1
	for wi in ntf:
		coslen += wi^2
		if wi > mtf:
			mtf = wi
		
	coslen = math.sqrt(coslen)
	
	cosnorm.append(coslen)
	
	# The lines above, for every document, calculate the cosine normalization and append it to a list
	# The line below calculated the maximum term frequency for a given document and stores it to a list
	maxtf.append(mtf)

       








# The following outputs all the needed items for a TF posting list



pickle.dump(dict, open("post.p", "wb"))
pickle.dump(tf, open("tf.p", "wb"))
pickle.dump(id2id, open("idtoid.p", "wb"))
pickle.dump(docfreq, open("df.p", "wb"))
pickle.dump(cosnorm, open("cosn.p", "wb"))
pickle.dump(maxtf, open("maxtf.p", "wb"))


