import qexpandWpca
import queryexpand
import pickle
import pca_features
import basic_clustering
import tfkm
import do_tfidf
import doPrint

#(modified from basic_clustering.py k_means_retrieval at line 109. Indention looks off)


def pca_clust_retrieval(theQvec, clusters, cluster_centroid, fil, style, qnum):

    centroid_helper = pickle.load(open("centroid_helper.p", "rb"))

    docfreq = pickle.load(open("df.p", "rb"))

    id2 = pickle.load(open("idtoid.p", "rb"))

    N = len(id2)

    cosnorm = pickle.load(open("cosn.p", "rb"))

    closeClusters = tfkm.getKcc_style(theQvec, cluster_centroid, 1, style)
    #print(query)
    #print(closeClusters)

    #print(clusters[closeClusters[0]])
    #print(theQvec)
    #print(centroid_helper[clusters[closeClusters[0]][0]])

    retrieval_list = {}
    for document in clusters[closeClusters[0]]:
        #print(episode_numbers[document])
        score = do_tfidf.lncltn_score(theQvec, centroid_helper[document], docfreq, N, cosnorm[document])
        if style == 2:
            score = do_tfidf.bnnbnn_score(theQvec, centroid_helper[document])
        elif style == 3:
            cn2 = tfkm.cnorm(theQvec)
            score = do_tfidf.ancapc_score(theQvec, centroid_helper[document], docfreq, N, cosnorm[document], cn2, maxtermf[document])
        retrieval_list[document + 1] = score

    retrieval_list = sorted(retrieval_list.items(), key=lambda x: x[1] * -1)

    #print(retrieval_list)

    index = 0
    list1 = []
    list2 = []
    #print(scoredic)
    for values in retrieval_list:
        index +=1
        if ((index <= 100)):
            list1.append(values[0])
            list2.append(values[1])




    # print those top 100 items added to the list in trec format


    doPrint.treccarPrint(list1, str(qnum), "pca-feature", fil, list2)


    return retrieval_list



def pca_main():

    filnams = ['km1lncltnq1.run','km1bnnbnnq1.run','km1ancapcq1.run','km1lncltnq2.run','km1bnnbnnq2.run','km1ancapcq2.run','km1lncltnq3.run','km1bnnbnnq2.run','km1ancapcq3.run']

    outfilnams = ['pcaC1lncltn.run', 'pcaC1bnnbnn.run', 'pcaC1ancapc.run','pcaC2lncltn.run', 'pcaC2bnnbnn.run', 'pcaC2ancapc.run','pcaC3lncltn.run', 'pcaC3bnnbnn.run', 'pcaC3ancapc.run']

    qlistlist = ['Qtex.p','Qtex.p','Qtex.p','Q2tex.p','Q2tex.p','Q2tex.p','Q3tex.p','Q3tex.p','Q3tex.p',]


    #	In a function:
    #	(from qexpandWpca.py lines 139 to 147. Slightly modified to refer to qexpandWpca, and #replace 100 with k)
    
    k = 100

    pcamat = qexpandWpca.loadupfeature(k)

    #print(pcamat)

    centroid_helper = pickle.load(open("centroid_helper.p", "rb"))

    allvecs = pickle.load(open("centroid_helper.p", "rb"))

    

    vecs = qexpandWpca.PCAtoallVecs(allvecs, pcamat)
    
    #	(from tfpca.py lines 49 to 54)
    
    centroids = []
    
    for x in range(k):
        xdic = {}
        xdic[x] = 1
        centroids.append(xdic)

    #Original code

    Feature_averages = []

    for x in range(k):
        Xsum = 0
        for y in range(len(vecs)):
            Xsum += vecs[y][x]
            Feature_averages.append(Xsum / len(vecs))

    clusters = {}

    for x in range(k):
        clusters[x] = []
        for y in range(len(vecs)):
            if vecs[y][x] > Feature_averages[x]:
                clusters[x].append(y)


    for alpha in range(len(filnams)):
        style = alpha % 3
        allq = pickle.load(open(qlistlist[alpha],'rb'))
        #General code
        qlist = qlistlist[alpha]
        queries = pickle.load(open(qlist, 'rb'))

        sind = alpha % 3
        outstr = outfilnams[alpha]

        ofil = open(outstr, 'w')
        ofil.close()


        #(from qexpandWpca.py lines 161 and 163 and 135 and 160)

		#filnams = ['ancapc.run']

        nam = filnams[alpha]

        rt4all = queryexpand.getRetrieved(nam)

        rt4most = queryexpand.justtopk(rt4all, 10)

        #(line 165 - 169)

        for qn in rt4most.keys():

            slice = queryexpand.getslice(vecs, rt4most[qn])

            expandterms = basic_clustering.centroid_func(slice)

            #Original code:

            newquery = tfkm.tovecform(queries[int(qn)])

            #(modified code from basic_clustering.py line 226)

            pca_clust_retrieval(newquery, clusters, centroids, outstr, style, qn)

if __name__ == '__main__':
	pca_main()
