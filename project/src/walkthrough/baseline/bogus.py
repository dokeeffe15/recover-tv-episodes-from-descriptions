
import pickle
# this is the bogus / trivial base line method
def bogus(query):
	idkey = pickle.load(open("id.p","rb"))
	alldocs = pickle.load(open("tex.p","rb"))
	
	tokenzed = query.split()
	
	ranking = []
	
	index = 0
	# this is an extremely simple (and slow) boolean AND on the first 2 terms in the query
	for doc in alldocs:
		if (str(tokenzed[0]) in doc) and (str(tokenzed[1]) in doc):
			ranking.append(idkey[index])
		index = index + 1
	
	#print(idkey)
	#print(alldocs)
	return ranking

if __name__ == '__main__':
	present = bogus("Joey dates someone else")

	print(present)
	
	# This is a test of the bogus method to make sure it was returning something
