
import pickle

from bogus import bogus

import ndcg_india




relevant = pickle.load(open("Qid.p","rb"))
queries = pickle.load(open("Qtex.p","rb"))

final = len(queries) - 1

#print(relevant[final])
#print(queries[final])


index = 0

ndcgscores = []

for query in queries:
	
	# for every query, run the bogus method on the query and see what we get back
	present = bogus(query)
	
	# relevant of [index] will give the relevant documents assuming the relevant documents were stored properly in query creation
	# ndcg india 's version of ndcg can be used on each query and the results appended to a list
	scor = ndcg_india.ndcg(relevant[index], present)
	index = index + 1
	
	ndcgscores.append(scor)

print("NDCG scores for each query for bogus retrieval")

print(ndcgscores)
# print the ndcg score and print their average. 
av = sum(ndcgscores)/len(ndcgscores)
print("average of NDCG scores for each query of bogus retrieval")
print(av)

print("total queries run:")
print(len(ndcgscores))

