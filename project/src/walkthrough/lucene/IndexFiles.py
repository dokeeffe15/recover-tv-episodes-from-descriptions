import lucene

print(lucene.VERSION)

print("lucene confirmed")
from trec_car.read_data import *
import argparse
# Code below thanks to https://svn.apache.org/viewvc/lucene/pylucene/trunk/samples/
# and to https://github.com/thr0n/PyLucene-Demo/blob/master/example/lucene_demo.py

INDEX_DIR = "IndexFiles.index"

import pickle

import sys, os, lucene, threading, time
from datetime import datetime

from java.nio.file import Paths
from org.apache.lucene.analysis.miscellaneous import LimitTokenCountAnalyzer
from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.document import Document, Field, FieldType
from org.apache.lucene.index import \
    FieldInfo, IndexWriter, IndexWriterConfig, IndexOptions
from org.apache.lucene.store import SimpleFSDirectory
# from org.apache.lucene.store import SimpleFSDirectory

from org.apache.lucene.util import Version

NoT = 100000  # Number of Tokens


class IndexFiles(object):

    def __init__(self, root, storeDir, analyzer):

        # Create a directory called storeDir to store the index
        if not os.path.exists(storeDir):
            os.mkdir(storeDir)

        # set up the analyzer right, then set the writer to write to storeDir
        storeDirectory = SimpleFSDirectory(Paths.get(storeDir))
        #print("got here")

        # the line below should no be needed if analyzer is passed in
        # analyzer = StandardAnalyzer()
        analyzer = LimitTokenCountAnalyzer(analyzer, NoT)
        #print("got here")
        config = IndexWriterConfig(analyzer)
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE)
        writer = IndexWriter(storeDirectory, config)

        # the following line tells the computer to execute indexDocs using the configured writer above
        self.indexDocs(root, writer)
        #print("commit index")
        writer.commit()
        writer.close()
        print("index done")

    def indexDocs(self, root, writer):
        fType = FieldType()
        fType.setStored(True)
        fType.setTokenized(False)
        fType.setIndexOptions(IndexOptions.DOCS_AND_FREQS)

        ft2 = FieldType()
        ft2.setStored(False)
        ft2.setTokenized(True)
        ft2.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS)

        # These lines of code go to the root, then go to each file and directory off of the root
       # for root, dirnames, filenames in os.walk(root):
        if True:
            #for filename in filenames:
            if True:
                # The following line of code takes only .txt files
                # These lines of code should be changed to handle non .txt files using trec-car-tools




                #if not filename.endswith('.cbor'):
                    #continue


                #print("adding")
                #print(filename)
                path = os.path.join(root)
                #print("red")
                #file = open(path, 'rb')

                episode_text = pickle.load(open(path, 'rb'))
                z = 0
                epiid = pickle.load(open('id.p','rb'))


                #file.close()
                #print("orange")
                try:
                    for txt in episode_text:
                        doc = Document()
                        doc.add(Field("paragraph ID", epiid[z], fType))
                        z = z + 1
                        if len(txt) > 0:
                            doc.add(Field("contents", txt, ft2))
                        else:
                            print("no content!")
                        doc.add(Field("txt", txt, fType))
                        writer.addDocument(doc)

                except Exception as e:
                    print("failed!")
                    print(e)
                #file.close()

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("not enough args")
        print(IndexFiles.__doc__)
        sys.exit(1)

    lucene.initVM(vmargs=['-Djava.awt.headless=true'])
    # lucene.initVM()
    print(lucene.VERSION)
    print(Version.LUCENE_CURRENT)
    print("got here")

    try:
        base_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
        IndexFiles(sys.argv[1], os.path.join(base_dir, INDEX_DIR),
                   StandardAnalyzer())
    except Exception as e:
        print("fail")
        print(e)

# from trec_car.read_data import *
# import argparse

# some lines of code "borrowed" from test.py, from: https://github.com/TREMA-UNH/trec-car-tools/tree/v2.1/python3
# proper citation coming soon.


# unpack test 200

# extract full text of paragraph

# interface with lucene indexer. full text on all paragraphs and paragraph ID.


# python IndexFiles.py ./tex.p



