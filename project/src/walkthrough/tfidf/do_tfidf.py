
# This file will run tf-idf methods on test 200 and write the results to .run file
# The tf-idf methods are: bnn.bnn, lnc.ltn, and anc.apc

import pickle

from trec_car.read_data import *
import argparse
import Stemmer

import sys
import math

import India_tokenize
import doPrint

# The following lines load in the parts of the posting list and term frequency information

# converts docID back to paragraph ID. id2[int(docID) - 1] = paragraphID.   the indexes IS off by 1. id2[int(docID) - 1]
# PLEASE SUBTRACT 1 from index
id2 = pickle.load(open("idtoid.p", "rb"))

# dictionary, that for every word there is a list of term frequencies, in the same order as the list of documents in the posting list. indexes match posting list.
# termfreq[word] = list of term frequenices.
# termfreq[word][index] = term frequency for postinglist[word][index]
# if a document ID is not in the postinglist we assume its term frequency is 0
termfreq = pickle.load(open("tf.p", "rb"))

# simple dictionary related term to how many docs it appears in.
# docfreq[word] = how many documents does the word appear in
docfreq = pickle.load(open("df.p", "rb"))

# inverted index. dictionary. postinglist[word] = list of all documents that have that word (by "docID")
postinglist = pickle.load(open("post.p", "rb"))

# The cosine normalization (length) of every document. The indxes are off by 1 like the id key. 
# cosnorm[docID - 1] = the length / cosine normalization of the document
cosnorm = pickle.load(open("cosn.p", "rb"))

# for each document, this returns the highest term frequency of all terms in that document. This idnex is off by 1.
# maxtermf[docID - 1] = tf of the word with the highest tf in that doc
maxtermf = pickle.load(open("maxtf.p", "rb"))

# to check more on what each loaded thing is, uncomment these print statements
#print(postinglist)
#print(docfreq)
#print(termfreq)
#print(id2)

#print(cosnorm)

#print(id2[1849 - 1])

# This is a dummy query designed to test everything is working
query = ["renaiss", "microprocessor", "anticodon", "member", "war"]


# lnc.ltn
# Query is a list which contains all the terms in the query
# termfreq is the same as termfreq found above: a double dictionary that gives the tf for any term and document
# docfreq is the same as docfreq above: a dictionary that gives the document frequency for any term
# posting list is the same as postinglist above: a dictionary that gives an inverted index of documents for every term
# tfqueries is a list that, for every item in the query list, gives the frequency of that term as it appears in the queries
# N is the number of documents in the collection
# cosnorm is a list which for every document contains its cosine normalzing length
def lncltn(query, termfreq, docfreq, postinglist, tfqueries, N, cosnorm):

	# This dictionary will return a lnc.ltn score for each document in the collection
	dictw = {}

	for term in query:

		if term in postinglist:
			if (term in postinglist) and (len(postinglist[term]) > 0):

				for ID in postinglist[term]:

					if ID not in dictw:
						dictw[ID] = 0

					score = (1 + math.log(termfreq[term][ID])) * (1) * (1/cosnorm[ID - 1]) * (1 + math.log(tfqueries[term]) * (math.log(N/docfreq[term])) * (1))
					# in lnc.ltn the score is the log of term frequencies, times the cosine normalizaition, times the queries log TF, times the queries inverse document frequency

					dictw[ID] = score + dictw[ID]


	return dictw


# bnn.bnn
# Query is a list which contains all the terms in the query
# posting list is the same as postinglist above: a dictionary that gives an inverted index of documents for every term
def bnnbnn(query, postinglist):
	# This dictionary will return a bnn.bnn score for each document in the collection
	docsnotzero = {}

	for term in query:
		if (term in postinglist) and (len(postinglist[term]) > 0):
			for ID in postinglist[term]:

				if ID not in docsnotzero:
					docsnotzero[ID] = 0

				score = 1
				# in bnn.bnn the score is simply the sum of terms that are in both the documents and the query

				docsnotzero[ID] = score + docsnotzero[ID]






#	docsnotzero[docID] = score + docsnotzero[docID]


	return docsnotzero





# anc.apc
# Query is a list which contains all the terms in the query
# termfreq is the same as termfreq found above: a double dictionary that gives the tf for any term and document
# docfreq is the same as docfreq above: a dictionary that gives the document frequency for any term
# posting list is the same as postinglist above: a dictionary that gives an inverted index of documents for every term
# tfqueries is a list that, for every item in the query list, gives the frequency of that term as it appears in the queries
# N is the number of documents in the collection
# cosnorm is a list which for every document contains its cosine normalzing length
# maxtf is maxtermf above, a list that for every document contains the maximum term frequency within that document
def ancapc(query, termfreq, docfreq, postinglist, tfqueries, N, cosnorm, maxtf):

	# This dictionary will return a anc.apc score for each document in the collection

	dictw = {}


	cosnormquery = 0

	for qtf in tfqueries.values():
		cosnormquery = pow(qtf, 2)
	cosnormquery = math.sqrt(cosnormquery)


	for term in query:
		if (term in postinglist) and (len(postinglist[term]) > 0):
			for ID in postinglist[term]:

				if ID not in dictw:
					dictw[ID] = 0

				score = ((0.5 + ((0.5 * termfreq[term][ID]) / (maxtf[ID-1]))) * (1) * (1/(cosnorm[ID -1])) * (.5+(.5*tfqueries[term])/max(tfqueries.values())) * (1/(cosnormquery)))
				
				# in anc.apc the score is the augmented tf of the doc * cosine normalization * augmented query * cos normalization
				
				# the prob idf needs to be handles, because this is reliant on a max value between 2 objects, it was handled as an if statement

				probidf = math.log((N - docfreq[term])/docfreq[term])

				if probidf > 0:
					score = score * probidf
				else:
					score = 0


				dictw[ID] = score + dictw[ID]

	return dictw
'''
tfqueries = {}
for term in query:
	tfqueries[term] = 1
	

answr = bnnbnn(query, postinglist)
answr2 = lncltn(query, termfreq, docfreq, postinglist, tfqueries, len(id2), cosnorm)
answr3 = ancapc(query, termfreq, docfreq, postinglist, tfqueries, len(id2), cosnorm, maxtermf)

print(answr)
print(answr2)
print(max(answr2.values()))
print(answr3)
print(max(answr3.values()))
'''

# the following just computes the scoring part of the lncltn tf-idf, given a document and query in vector form
def lncltn_score(queryD, docD, docfreq, N, cosnorm):
	score = 0
	for [word, value] in queryD.items():
		if word in docD:
			rv = 1

			if float(docD[word]) > 0:
				left_v = (1 + math.log(float(docD[word])))
				left_v = left_v / float(cosnorm)
			else:
				left_v = 0
			
			#print(left_v)
			temp = value
			if value < 0:
				rv = 0
			else:	
				if ((docfreq[word] <= 0) or (docfreq[word] >= N)):
					rv = 0
				else:
					rv = rv * (1 + math.log(float(temp)))
					rv = rv * (math.log(N/docfreq[word])) * (1)
			score = score + left_v*rv
			
	return score

# the following just computes the scoring part of the bnnbnn tf-idf, given a document and query in vector form
def bnnbnn_score(queryD, docD):
	score = 0
	for [word, value] in queryD.items():
		if word in docD:
			left =  1 
			right = 1
			score = score + left*right
			
	return score

# the following just computes the scoring part of the ancapc tf-idf, given a document and query in vector form
def ancapc_score(queryD, docD, docfreq, N, cosnorm1, cosnorm2, maxtf):
	score = 0
	for [word, value] in queryD.items():
		

		
		if word in docD:
			left =  (0.5 + ((0.5 * float(docD[word])) / (maxtf))) * (1) * (1/cosnorm1) 
			right = (0.5 + ((0.5 * value) / (maxtf))) * (1/cosnorm2) 
			
			probidf = 0
			if ((docfreq[word] <= 0) or (docfreq[word] >= N)):
				probidf = 0
			else:
				probidf = math.log((N - docfreq[word])/docfreq[word])

			if probidf > 0:
				right = right * probidf
			else:
				right = 0
			
			score = score + left*right
			
	return score




if __name__ == '__main__':

	#thearg = sys.argv[1]
	
	if ((len(sys.argv) < 2) or str(sys.argv[1]) == '1'):
		all_queries = pickle.load(open("Qtex.p","rb"))
		futurefiles = ['bnnbnn.run', 'lncltn.run', 'ancapc.run']
	elif str(sys.argv[1]) == '2':
		all_queries = pickle.load(open("Q2tex.p","rb"))
		futurefiles = ['bnnbnn2.run', 'lncltn2.run', 'ancapc2.run']
	elif str(sys.argv[1]) == '3':
		all_queries = pickle.load(open("Q3tex.p","rb"))
		futurefiles = ['bnnbnn3.run', 'lncltn3.run', 'ancapc3.run']
	else:
		all_queries = pickle.load(open("Qtex.p","rb"))
		futurefiles = ['bnnbnn.run', 'lncltn.run', 'ancapc.run']

	#print(thearg)
# THESE LINES OF CODE HANDLE bnn.bnn
	file = open(futurefiles[0], "w")
	file.close()

	#args = open(thearg, 'rb')

	qnum = 0;

	for query in all_queries:

		stopword = India_tokenize.getstopword()
		# for each query tokenize the query

		querywords = India_tokenize.tokenize_india(query, stopword)
		# then pass to the right tf-idf function

		bnnbnnranking = bnnbnn(querywords, postinglist)

		bnnbnnranking_sorted = sorted(bnnbnnranking.items(), key=lambda x: x[1] * -1)

		index = 0
		list1 = []
		list2 = []
		# for the top 100 items of the rankings in reverse sorted order (highest score) add to a list
		for values in bnnbnnranking_sorted:
			index +=1
			if index <= 100:
				list1.append(values[0])
				list2.append(values[1])





		doPrint.treccarPrint(list1, str(qnum), "bnnbnn", futurefiles[0], list2)
		
		qnum = qnum + 1
		
		# print those top 100 items added to the list in trec format


# THESE LINES OF CODE HANDLE lnc.ltn
	file = open(futurefiles[1], "w")
	file.close()

	#args = open(thearg, 'rb')
	qnum = 0;
	for query in all_queries:

		stopword = India_tokenize.getstopword()
		# for each query tokenize the query

		querywords = India_tokenize.tokenize_india(query, stopword)

		tfqueries = {}
		for terms in querywords:
			if terms not in tfqueries:
				tfqueries[terms] = 1
			else:
				tfqueries[terms] += 1

		lncltnranking = lncltn(querywords, termfreq, docfreq, postinglist, tfqueries, len(id2), cosnorm)
		# then pass to the right tf-idf function

		lncltnranking_sorted = sorted(lncltnranking.items(), key=lambda x: x[1] * -1)

		index = 0
		list1 = []
		list2 = []
		# for the top 100 items of the rankings in reverse sorted order (highest score) add to a list

		for values in lncltnranking_sorted:
			index +=1
			if index <= 100:
				list1.append(values[0])
				list2.append(values[1])




		# print those top 100 items added to the list in trec format


		doPrint.treccarPrint(list1, str(qnum), "lncltn", futurefiles[1], list2)
		
		qnum = qnum + 1

# THESE LINES OF CODE HANDLE anc.apc
	file = open(futurefiles[2], "w")
	file.close()

	#args = open(thearg, 'rb')
	
	qnum = 0;

	for query in all_queries:

		stopword = India_tokenize.getstopword()
		# for each query tokenize the query

		querywords = India_tokenize.tokenize_india(query, stopword)

		tfqueries = {}
		for terms in querywords:
			if terms not in tfqueries:
				tfqueries[terms] = 1
			else:
				tfqueries[terms] += 1

		ancapcranking = ancapc(querywords, termfreq, docfreq, postinglist, tfqueries, len(id2), cosnorm, maxtermf)
		# then pass to the right tf-idf function

		ancapcranking_sorted = sorted(ancapcranking.items(), key=lambda x: x[1] * -1)

		index = 0
		list1 = []
		list2 = []
		# for the top 100 items of the rankings in reverse sorted order (highest score) add to a list

		for values in ancapcranking_sorted:
			index +=1
			if index <= 100:
				list1.append(values[0])
				list2.append(values[1])



		# print those top 100 items added to the list in trec format



		doPrint.treccarPrint(list1, str(qnum), "ancapc", futurefiles[2], list2)
		
		qnum = qnum + 1
