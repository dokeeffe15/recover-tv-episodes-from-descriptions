# the following function is designed to use km clustering as an additional feature to compute tf-idf

import pickle

import India_tokenize

import basic_clustering

import do_tfidf

import doPrint_alt

import math

import qexpandWpca


# the following function takes any string and converts it into an appropriate vector notation
def tovecform(astring):
	
	stopword = India_tokenize.getstopword()
	tokenlist = India_tokenize.tokenize_india(astring, stopword)

	dic = {}

	for tokn in tokenlist:
		if tokn not in dic:
			 dic[tokn] = 1
		else:
			dic[tokn] += 1

	return dic

# the following computes the cosine normalization of any vector
def cnorm(vec):
	
	val = 0
	
	for word in vec.keys():
		val += (vec[word] * vec[word])
	
	val = math.sqrt(val)
	
	return val

# get the k closest clusters to the query, given the query in vector form and all the centroids in vector form
def getKcc_style(qvec, centroids, k, style):
	index = -1
	
	docfreq = pickle.load(open("df.p", "rb"))
	id2 = pickle.load(open("idtoid.p", "rb"))
	N = len(id2)
	
	closelist = []
	scorelist = []
	
	for center in centroids:
		index += 1
		centercosnorm = cnorm(center)
		doccosnorm = cnorm(qvec)
		score = do_tfidf.lncltn_score(qvec, center, docfreq, N, doccosnorm)
		if (style == 1):
			score = do_tfidf.bnnbnn_score(qvec, center)
		elif style == 2:
			score = do_tfidf.ancapc_score(qvec, center, docfreq, N, doccosnorm, centercosnorm, max(qvec.values()))
		scorelist.append(score)
	
	for x in range(k):
	
		smax = max(scorelist)
		clustnum = scorelist.index(smax)
		
		closelist.append(clustnum)
		
		scorelist[clustnum] = scorelist[clustnum] - 10
	
	return closelist

# get the k closest clusters to the query, specificially in lncltn, a more general function exists
def getKcc_lncltn(qvec, centroids, k):
	index = -1
	docfreq = pickle.load(open("df.p", "rb"))
	id2 = pickle.load(open("idtoid.p", "rb"))
	N = len(id2)	

	
	closelist = []
	scorelist = []
	
	for center in centroids:
		index += 1
		centercosnorm = cnorm(qvec)
		score = do_tfidf.lncltn_score(qvec, center, docfreq, N, centercosnorm)
		scorelist.append(score)
	
	for x in range(k):
	
		smax = max(scorelist)
		clustnum = scorelist.index(smax)
		
		closelist.append(clustnum)
		
		scorelist[clustnum] = scorelist[clustnum] - 10
	
	return closelist



# the following is to take in a style of tf-idf and use it to determine which centroid / cluster best matches an episode
# however, it can also be used to compute which of any number of vectors best matches another vector, including clusters, episodes, and queries
def style_compare(centroids, episode, epnum, style):
	index = -1
	
	#for each episode
	
	docfreq = pickle.load(open("df.p", "rb"))
	maxtermf = pickle.load(open("maxtf.p", "rb"))
	cosnorm = pickle.load(open("cosn.p", "rb"))
	id2 = pickle.load(open("idtoid.p", "rb"))
	N = len(id2)
	
	# score list
	scores = {}
	
	# for each centroid
	for centroid in centroids:
		index += 1
		# score_list.append(tfidf score)
		score = do_tfidf.lncltn_score(centroid, episode, docfreq, N, cosnorm[index])
		if style == 1:
			score = do_tfidf.bnnbnn_score(centroid, episode)
		
		if style == 2:
			score = do_tfidf.ancapc_score(centroid, episode, docfreq, N, cosnorm[index], cnorm(centroid), maxtermf[index])
		
			
		scores[epnum[index]] = score
	
	scores = sorted(scores.items(), key=lambda x: x[1] * -1)
	
	return scores






def lncltn_compare(centroids, episode, epnum):
	index = -1
	
	#for each episode
	docfreq = pickle.load(open("df.p", "rb"))
	maxtermf = pickle.load(open("maxtf.p", "rb"))
	cosnorm = pickle.load(open("cosn.p", "rb"))
	id2 = pickle.load(open("idtoid.p", "rb"))
	N = len(id2)
	
	# score list
	scores = {}
	
	# for each centroid
	for centroid in centroids:
		index += 1
		# score_list.append(tfidf score)
		score = do_tfidf.lncltn_score(centroid, episode, docfreq, N, cosnorm[index])
		scores[epnum[index]] = score
	
	scores = sorted(scores.items(), key=lambda x: x[1] * -1)
	
	return scores

# the following function is designed to, when given a specific query, 
#compute the closest cluster centroids to the query
#add those cluster centroids with various weights to the query vector
# computer the tf-idf score of every episode on the new query and return the top results
def handleQ(query, qnum, fil, style):
	
	theQvec = tovecform(query)
	
	if style == 0:
		#closeClusters = getKcc_lncltn(theQvec, centroids, 3)		
		closeClusters = getKcc_style(theQvec, centroids, 3, style)
	else:
		closeClusters = getKcc_style(theQvec, centroids, 3, style)
	
	#print(closeClusters)
	
	weight = len(theQvec) / 2
	
	subt = weight / 4
	
	for cn in closeClusters:
		
		theQvec[cn] = weight
		
		weight = weight - subt
	
	#print(theQvec)
	
	
	scoredic = lncltn_compare(vecs, theQvec, epnum)
	if ((style == 1) or (style == 2) or (style == 0)):
		scoredic = style_compare(vecs, theQvec, epnum, style)

	index = 0
	list1 = []
	list2 = []
	#print(scoredic)
	for values in scoredic:
		index +=1
		if ((index <= 100) and (values[1] > 0)):
			list1.append(values[0])
			list2.append(values[1])




		# print those top 100 items added to the list in trec format


	doPrint_alt.treccarPrint(list1, str(qnum), "tf_w_km", fil, list2)
	
	
	
	return True


# the following function is designed to, when given a specific query, 
#compute the closest cluster centroids to the query
#add those cluster centroids with various weights to the query vector
# computer the tf-idf score of every episode on the new query and return the top results
def handleQ2(theQvec, qnum, fil, style, centroids, vecs):
	
	epnum = pickle.load(open('id.p','rb'))
	
	if style == 0:
		#closeClusters = getKcc_lncltn(theQvec, centroids, 3)
		closeClusters = getKcc_style(theQvec, centroids, 3, style)
	else:
		closeClusters = getKcc_style(theQvec, centroids, 3, style)
	
	#print(closeClusters)
	
	weight = len(theQvec) / 2
	
	subt = weight / 4
	
	for cn in closeClusters:
		
		theQvec[cn] = weight
		
		weight = weight - subt
	
	#print(theQvec)
	
	
	scoredic = lncltn_compare(vecs, theQvec, epnum)
	if ((style == 1) or (style == 2) or (style == 0)):
		scoredic = style_compare(vecs, theQvec, epnum, style)

	index = 0
	list1 = []
	list2 = []
	#print(scoredic)
	for values in scoredic:
		index +=1
		if ((index <= 100) and (values[1] > 0)):
			list1.append(values[0])
			list2.append(values[1])




		# print those top 100 items added to the list in trec format


	doPrint_alt.treccarPrint(list1, str(qnum), "tf_w_c", fil, list2)
	
	
	
	return True

# the following function is designed to, when given a specific query, 
#compute the closest cluster centroids to the query
#add those cluster centroids with various weights to the query vector
# computer the tf-idf score of every episode on the new query and return the top results
def handleQ2b(theQvec, qnum, fil, style, vecs):
	
	epnum = pickle.load(open('id.p','rb'))
	
	
	
	#print(theQvec)
	
	
	scoredic = style_compare(vecs, theQvec, epnum, style)
	if ((style == 1) or (style == 2) or (style == 0)):
		scoredic = style_compare(vecs, theQvec, epnum, style)

	index = 0
	list1 = []
	list2 = []
	#print(scoredic)
	for values in scoredic:
		index +=1
		if ((index <= 100)):
			list1.append(values[0])
			list2.append(values[1])




		# print those top 100 items added to the list in trec format


	doPrint_alt.treccarPrint(list1, str(qnum), "tf_w_c", fil, list2)
	
	
	
	return True


if __name__ == '__main__':
	#handleQ('Constable Odo, Gamma Quadrant, Deep Space Nine')
	

	vecs = pickle.load(open('centroid_helper.p','rb'))
	epnum = pickle.load(open('id.p','rb'))
	epdata = pickle.load(open('tex.p','rb'))
	#clusters = pickle.load(open('kmcluster.p','rb'))




	docfreq = pickle.load(open("df.p", "rb"))
	maxtermf = pickle.load(open("maxtf.p", "rb"))
	cosnorm = pickle.load(open("cosn.p", "rb"))
	id2 = pickle.load(open("idtoid.p", "rb"))
	N = len(id2)

	#print(vecs)



	#print(epnum)



	#print(epdata)


	#print(clusters)

	# The following adds the cluster to the vector representation
	
	clustlistlist = [3, 25, 100]
	
	for c in clustlistlist:
		cstr = str(c)

		
		#print(centroids[0])

		#print(vecs[20])
		#print(epnum[20])
		#print(epdata[20])






		
		# for every combination of the number of clusters, datassets, and tf-idf methods
		# first the kmeans clusters are computed
		# then for every query the query is processed by the query handling function.
		
		
		qlistlist = ['Qtex.p', 'Q2tex.p', 'Q3tex.p']
		outlist = ['tfwkm.run', 'tfwkm2.run', 'tfwkm3.run']
		sstr = ['lncltn', 'bnnbnn', 'ancapc']
		index = -1
		
		for qlist in qlistlist:
			index += 1
			
			for sind in range(3):
				
				#print(sind)
				
				clusters = basic_clustering.k_means(c, sind+1)

				centroids = []

				aclusterdic = {}
				
				for clust in clusters.keys():
					
					aclustercount = 0
					
					slice = []
					for ep in clusters[clust]:
						vecs[ep][clust] = 1
						aclustercount += 1
						slice.append(vecs[ep])
					centroids.append(basic_clustering.centroid_func(slice))
					
					aclusterdic[clust] = aclustercount
					
					docfreq[clust] = len(clusters[clust])
					maxtermf[clust] = 10
					

				qexpandWpca.addemtodocfreq(aclusterdic)
			
				outstr = sstr[sind] +  cstr + outlist[index]
				
				outfil = open(outstr, 'w')
				outfil.close()
				
				queries = pickle.load(open(qlist, 'rb'))
				
				qnum = 0
				for query in queries:
					handleQ(query, qnum, outstr, sind)
					qnum += 1
			
			
			
			

