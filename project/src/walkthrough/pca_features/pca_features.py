#PCA features only:

import qexpandWpca
import queryexpand
import pickle
import basic_clustering
import tfkm
import do_tfidf
import doPrint

#Origin code

def removenonints(dic):
	Newdic = {}
	for key in dic.keys():
		if type(key) == int:
			Newdic[key] = dic[key]
	return Newdic


def pca_feature_main():
	#In a function:
	#(from qexpandWpca.py lines 139 to 147. Slightly modified to refer to qexpandWpca, #and replace 100 with k)

	k = 100

	pcamat = qexpandWpca.loadupfeature(k)

	#print(pcamat)

	allvecs = pickle.load(open("centroid_helper.p", "rb"))
	
	vecs = qexpandWpca.PCAtoallVecs(allvecs, pcamat)
	
	filnams = ['km1ancapcq1.run','km1ancapcq2.run','km1ancapcq3.run']

	outfilnams = [ 'pcaF1ancapc.run', 'pcaF2ancapc.run', 'pcaF3ancapc.run']

	qlistlist = ['Qtex.p','Q2tex.p','Q3tex.p']

	for alpha in range(len(filnams)):
		#General code
		qlist = qlistlist[alpha]
		queries = pickle.load(open(qlist, 'rb'))
		allq = pickle.load(open('Qtex.p','rb'))
		sind = 2
		outstr = outfilnams[alpha]

		ofil = open(outstr, 'w')
		ofil.close()

		#(from qexpandWpca.py lines 161 and 163 and 135 and 160)

		#filnams = ['ancapc.run']

		nam = filnams[alpha]

		rt4all = queryexpand.getRetrieved(nam)

		rt4most = queryexpand.justtopk(rt4all, 10)

		#(line 165 - 169)

		for qn in rt4most.keys():

			#print(qn)

			slice = queryexpand.getslice(vecs, rt4most[qn])

			expandterms = basic_clustering.centroid_func(slice)

			#Original code:

			newquery = removenonints(expandterms)

			#(line 184 modified)

			qexpandWpca.handleQf(newquery, qn, outstr, sind, allvecs)

if __name__ == '__main__':
	pca_feature_main()
