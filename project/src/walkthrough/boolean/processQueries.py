


from trec_car.read_data import *
import argparse
import Stemmer
from Merge_Algorithm import merge
import pickle
from doPrint import treccarPrint
from boolOR import mergeor
import sys

# This code was based off of the test.py file in trec car tools python
# the function handle dump_pages was recycled, but changed to be triggered on parameter "or"
# dump_pages is just the OR version of the AND query processing, recycling dump_outlines to read the outlines file


def dump_pages(args):
    stopword = []

    nameoffil = "booleanOR.run"
    fil = open(nameoffil, "w")
    fil.close()
# load the postings lists
    posting_list = pickle.load(open("posting.p", "rb"))

    thefile = open("Jstopword.txt", "r")
    for x in thefile:
        stopword.append(x.strip())
    thefile.close()
    stemmer = Stemmer.Stemmer('english')
# prepare to tokenize the query
    cw = 0
# for each query
    for p in iter_outlines(args.file):
        #   print(p.page_meta)
        #  print(p)
        # print("\n".join([("%s"% heading ) for (heading,empty_content) in p.deep_headings_list()]))

        Qwords = []
        word = ""
        for letter in p.page_name:
            # tokenize the query exactly as the data was tokenized
            if (ord(letter) >= 65) & (ord(letter) <= 90):
                # print("cap")
                letter = chr(ord(letter) + 32)
                word = word + letter
            elif (ord(letter) >= 97) & (ord(letter) <= 122):
                # print("lower")
                word = word + letter
            else:
                # print("done")

                if (len(word) <= 2):
                    word = ""
                else:
                    word = stemmer.stemWord(word)
                    if (len(word) <= 2):
                        word = ""
                    else:
                        # print(word)
                        cw = cw + 1
                        Qwords.append(word)
                        # fakeH.words.append(word)
                    word = ""
        if (len(word) > 2):
            word = stemmer.stemWord(word)
            if (len(word) > 2):
                Qwords.append(word)

        Qwords = list(set(Qwords))
        for text in stopword:
            if (len(stemmer.stemWord(text)) > 2) & (stemmer.stemWord(text) in Qwords):
                Qwords.remove(stemmer.stemWord(text))

        Qwords.sort()
# Qwords now contains all the tokenized terms in this given query
        resultlist = []
        if len(Qwords) > 0:

            if Qwords[0] not in posting_list:
                posting_list[Qwords[0]] = []
                #print("no document has word: ", Qwords[0])

            resultlist = posting_list[Qwords[0]]
# continiously use mergeor, the boolean OR algorithm to merge the lists
            for word in Qwords:
                if word not in posting_list:
                    posting_list[word] = []
                    #print("no document has word: ", word)
                resultlist = mergeor(resultlist, posting_list[word])

          

      #  print(p.page_id)
        treccarPrint(resultlist, p.page_id, "OR", nameoffil)


def dump_outlines(args):
    stopword = []
    
    nameoffil = "booleanAND.run"
    fil = open(nameoffil, "w")
    fil.close()

    posting_list = pickle.load(open("posting.p", "rb"))

    thefile = open("Jstopword.txt", "r")
    for x in thefile:
        stopword.append(x.strip())
    thefile.close()
    stemmer = Stemmer.Stemmer('english')

    cw = 0
# for each query
    for p in iter_outlines(args.file):
     #   print(p.page_meta)
      #  print(p)
       # print("\n".join([("%s"% heading ) for (heading,empty_content) in p.deep_headings_list()]))



        Qwords = []
        word = ""
        for letter in p.page_name:
            # tokenize the query
            if (ord(letter) >= 65) & (ord(letter) <= 90):
                # print("cap")
                letter = chr(ord(letter) + 32)
                word = word + letter
            elif (ord(letter) >= 97) & (ord(letter) <= 122):
                # print("lower")
                word = word + letter
            else:
                # print("done")

                if (len(word) <= 2):
                    word = ""
                else:
                    word = stemmer.stemWord(word)
                    if (len(word) <= 2):
                        word = ""
                    else:
                        # print(word)
                        cw = cw + 1
                        Qwords.append(word)
                        # fakeH.words.append(word)
                    word = ""
        if (len(word) > 2):
            word = stemmer.stemWord(word)
            if (len(word) > 2):
                Qwords.append(word)


        Qwords = list(set(Qwords))
        for text in stopword:
            if (len(stemmer.stemWord(text)) > 2) & (stemmer.stemWord(text) in Qwords):
                Qwords.remove(stemmer.stemWord(text))

        Qwords.sort()





		# continiously merge the postings lists of the query terms using the boolean AND merge

        resultlist = []
        if len(Qwords) > 0:


            if Qwords[0] not in posting_list:
                posting_list[Qwords[0]] = []
                #print("no document has word: ", Qwords[0])


            resultlist = posting_list[Qwords[0]]

            for word in Qwords:
                if word not in posting_list:
                    posting_list[word] = []
                    #print("no document has word: ", word)
                resultlist = merge(resultlist, posting_list[word])

            


       # print(p.page_id)
        treccarPrint(resultlist, p.page_id, "AND", nameoffil)






       # print(Qwords)
"""
    if saveunder not in posting_list:
        posting_list[saveunder] = []
        for thing in new_list:
            posting_list[saveunder].append(thing)
    else:
        print("save name failure")
"""

















def dump_paragraphs(args):
    print("not what this is used for. call with outlines.")
    #for p in iter_paragraphs(args.file):
    #    print(p)
        #z=input()
        #print(z)



import India_tokenize

import Merge_Algorithm

import doPrint

import boolOR

def run_and(picked, dna):
	stopword = India_tokenize.getstopword()
	
	queries = pickle.load(open(picked, "rb"))
	
	postinglist = pickle.load(open("post.p", "rb"))
	
	qn = 0
	
	fil = open(dna, "w")
	fil.close()
	
	for query in queries:
		listquery = India_tokenize.tokenize_india(query, stopword)
		
		if listquery[0] in postinglist:
			alpha_list = postinglist[listquery[0]]
		else:
			alpha_list = []
		
		for word in listquery:
			if word in postinglist:
				alpha_list = Merge_Algorithm.merge(alpha_list, postinglist[word])
			else:
				alpha_list = []
		
		scores = []
		
		for item in alpha_list:
			scores.append(1)
		
		doPrint.treccarPrint(alpha_list, str(qn), "and", dna, scores)
		
		qn = qn + 1
		
	
# the following code runs the merge or algorithm on every posting list of every word in the query
def run_or(picked, ro):
	stopword = India_tokenize.getstopword()
	
	queries = pickle.load(open(picked, "rb"))
	
	postinglist = pickle.load(open("post.p", "rb"))
	
	#print(postinglist)
	
	qn = 0
	
	fil = open(ro, "w")
	fil.close()
	
	for query in queries:
		listquery = India_tokenize.tokenize_india(query, stopword)
		
		if listquery[0] in postinglist:
			alpha_list = postinglist[listquery[0]]
		else:
			alpha_list = []
		# once the words in the query have been tokenized, the merge algorithm can be run on each of those tokenized words
		for word in listquery:
			if word in postinglist:
				alpha_list = boolOR.mergeor(alpha_list, postinglist[word])
		
		scores = []
		
		for item in alpha_list:
			scores.append(1)
		
		doPrint.treccarPrint(alpha_list, str(qn), "or", ro, scores)
		
		qn = qn + 1
		
	

if __name__ == '__main__':
   
   picked = sys.argv[1]
   dna = sys.argv[2]
   ro = sys.argv[3]
   
   run_and(picked, dna)
   
   run_or(picked, ro)
