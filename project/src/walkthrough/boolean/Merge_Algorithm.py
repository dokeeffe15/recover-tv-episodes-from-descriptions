import pickle

posting_list = pickle.load(open("post.p", "rb"))


# This is the merge algorithm for AND queries implemented based on pseudocode discussed in class
def merge(posting_listword1,posting_listword2):

   i = 0
   j = 0

   new_list = []

   while i < len(posting_listword1) and j < len(posting_listword2):
      if posting_listword1[i] == posting_listword2[j]:
         new_list.append(posting_listword1[i])
         i+=1
         j+=1
      elif posting_listword1[i] < posting_listword2[j]:
         i+=1
      else:
         j+=1

   return new_list


#print(merge(posting_list["censorship"],posting_list["bbc"]))

