
import pickle
import queryexpand
import basic_clustering
import tfkm
import math
import doPrint_alt


import numpy as np
import pickle
from sklearn.decomposition import PCA

# due to issues with document frequency when clusters are introduced, the following alters the pickle save of the index to add whatever is needed
def addemtodocfreq(somedic):
	
	docfreq = pickle.load(open("df.p", "rb"))
	
	for key in somedic.keys():
		docfreq[key] = somedic[key]
			
	pickle.dump(docfreq, open("df.p", "wb"))
	
	return True

# the following takes in an array of features, then reshapes that array for the queries before commencing PCA to reduce the features to less dimensions
def loadupfeature(k):
	array = pickle.load(open("array.p", "rb"))
	array = np.array(array).reshape(408,408)

	# doing PCA stuff
	pca = PCA(n_components=k)
	model = pca.fit_transform(array)
	
	
	return model

# the following function was designed to mimic handleQ but with less input arguments.
# the implementation is somewhat buggy, and has been removed from most uses.
def handleQsimple(theQvec, vecs, qnum, fil, style):
	
	epnum = pickle.load(open('id.p','rb'))
	
	scoredic = tfkm.lncltn_compare(vecs, theQvec, epnum)
	if ((style == 1) or (style == 2)):
		scoredic = tfkm.style_compare(vecs, theQvec, epnum, style)

	index = 0
	list1 = []
	list2 = []
	#print(scoredic)
	for values in scoredic:
		index +=1
		if ((index <= 100) and (values[1] > 0)):
			list1.append(values[0])
			list2.append(values[1])




		# print those top 100 items added to the list in trec format


	doPrint_alt.treccarPrint(list1, str(qnum), "tf_w_pca", fil, list2)
	
	
	
	return True



# the following takes in a vector representation of every episode, then adds its PCA features to that vector representation
def PCAtoallVecs(allvecs, pcamat):
	
	indexR = -1
	
	dfdic = {}
	# the PCA matrix is a large matrix where the rows are the episodes, and the columns are the features
	for row in pcamat:
		indexR += 1
		indexC = -1
		for col in pcamat[indexR]:
			indexC += 1
			
			allvecs[indexR][indexC] = pcamat[indexR][indexC] 
			
			# the following computes the "document frequency" of each feature to be saved
			if indexC not in dfdic:
				dfdic[indexC] = 0
			if pcamat[indexR][indexC] > 0:
				dfdic[indexC] += 1
			
	addemtodocfreq(dfdic)
			
			
	return allvecs


# the following is a successfully adapted version of handleQ
# this function takes in a query in vector form, and other arguments
# it then computes the retrieval score for every document, and prints the top documents to a file
def handleQf(theQvec, qnum, fil, style, vecs):
	
	epnum = pickle.load(open('id.p','rb'))
	


	
	# depending on the argument style the score is computed using a different tf-idf
	scoredic = tfkm.lncltn_compare(vecs, theQvec, epnum)
	if ((style == 1) or (style == 2)):
		scoredic = tfkm.style_compare(vecs, theQvec, epnum, style)

	index = 0
	list1 = []
	list2 = []
	#print(scoredic)
	for values in scoredic:
		index +=1
		if ((index <= 100) and (values[1] > 0)):
			list1.append(values[0])
			list2.append(values[1])




		# print those top 100 items added to the list in trec format


	doPrint_alt.treccarPrint(list1, str(qnum), "tf_w_km", fil, list2)
	
	
	
	return True





def doallqueries():
	# the following is designed for all combinations of query set and tf-idf to run handleQf on all queries
	filnams = ['km1ancapcq1.run','km1ancapcq2.run','km1ancapcq3.run']
	
	outfilnams = [ 'qexp1ancapcwpca.run', 'qexp2ancapcwpca.run', 'qexp3ancapcwpca.run']
	
	qlistlist = ['Qtex.p','Q2tex.p','Q3tex.p']
	
	
	pcamat = loadupfeature(100)
	
	#print(pcamat)
	
	allvecs = pickle.load(open("centroid_helper.p", "rb"))
	
	allq = pickle.load(open('Qtex.p','rb'))
	
	vecs = PCAtoallVecs(allvecs, pcamat)
	#vecs = allvecs
	
	# for every desired file combination
	for alpha in range(len(filnams)):
		outf = outfilnams[alpha]
		ofil = open(outf, 'w')
		ofil.close()
		
		#print(vecs)
		
		#print(vecs[0])
		
		lamb = 0.5
		
		nam = filnams[alpha]
		rt4all = queryexpand.getRetrieved(nam)
		
		rt4most = queryexpand.justtopk(rt4all, 10)
		
		for qn in rt4most.keys():
			# get the centroid of the top retrieved documents for a query
			slice = queryexpand.getslice(vecs, rt4most[qn])
			
			expandterms = basic_clustering.centroid_func(slice)
			
			#print(expandterms)
			# create a new query that's equal parts the old query, and the centroid of the results
			newquery = queryexpand.mnq(allq[int(qn)], expandterms, lamb)
			
			#print(newquery)
			
			style = 2
			
			fil = outfilnams[alpha]
			
			qnum = qn
			
			# for every query, run the new query and record the results
			#handleQsimple(newquery, vecs, qnum, fil, style)
			handleQf(newquery, qn, outf, style, allvecs)
		
		
			#print('-----------')
			#print(newquery[0])
			#print(vecs[0][0])
			
		
		
		
		
	return True


if __name__ == '__main__':
	doallqueries()

