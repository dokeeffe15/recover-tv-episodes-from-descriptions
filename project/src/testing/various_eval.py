
import ndcg_india

import pickle

def from_a_trec_run(filname, style):

	fil = open(filname)

	superflag = False
	prflag = False
	# when eveluating any given .run file first identify which query set is being used
	# load the relevant documents for every query. 
	if style == 1:
		rel_docs = pickle.load(open("Qid.p","rb"))
	elif style == 2:
		rel_docs = pickle.load(open("Q2id.p","rb"))
		prflag = True 
	else:
		rel_docs = pickle.load(open("Q3id.p","rb"))
		sup_rel_docs = pickle.load(open("Q3super.p","rb"))
		superflag = True
		prflag = True



	old_index = -1

	ndcg_scores = []
	
	patr_scores = []

	returned = []

	for line in fil:
		
		# for every line in the .run file which which query it is referring to. if this is a new query its time to compute ndcg on the returned results
		# if this is the same query as before, add the returned result to a list of returned results, careful to watch formatting so it matches the relevant documents format
		# if this is query set 3, that means some documents are more relevant than others, and the super relevant documents need to be handled seperately. 
		
		terms = line.split()
		
		if old_index == -1:
			old_index = terms[0]
		
		new_index = terms[0]
		if new_index == old_index:
			returned.append(terms[2].replace("_", " "))
		else:
			
			if len(rel_docs[int(old_index)]) < 1:
				print('\n *************** ')
				print(int(old_index))
			
			#print(returned)
			#print(rel_docs[int(old_index)])
			
			if superflag:
				score = ndcg_india.sndcg(rel_docs[int(old_index)], returned, sup_rel_docs[int(old_index)])
		
			else:
				score = ndcg_india.ndcg(rel_docs[int(old_index)], returned)
		
			ndcg_scores.append(score)
			
			if prflag:
				pscore = ndcg_india.patr(rel_docs[int(old_index)], returned)
				patr_scores.append(pscore)
			
		
			old_index = new_index
			
			returned = []
			returned.append(terms[2].replace("_", " "))
			
		
	completed = len(ndcg_scores)
	
	
		
	print(ndcg_scores)
	# print the ndcg score and print their average. 
	
	while len(ndcg_scores) < len(rel_docs.keys()):
		ndcg_scores.append(0)
	
	while len(patr_scores) < len(rel_docs.keys()):
		patr_scores.append(0)
	
	av = sum(ndcg_scores)/len(ndcg_scores)
	print("From the run file:")
	print(filname)
	print("average of NDCG scores for each query of this retrieval")
	print(av)

	print("total queries run:")
	print(completed)
	
	if prflag:
		print('\n P at R scores:')
		print(patr_scores)
		print('average: ')
		av = sum(patr_scores)/len(patr_scores)
		print(av)
	
	print('\n ---------------------------------------')

	fil.close()


if __name__ == '__main__':
	'''
	from_a_trec_run('ancapc3tfwkm.run', 1)
	from_a_trec_run('ancapc3tfwkm2.run', 2)
	from_a_trec_run('ancapc3tfwkm3.run', 3)
	from_a_trec_run('ancapc25tfwkm.run', 1)
	from_a_trec_run('ancapc25tfwkm2.run', 2)
	from_a_trec_run('ancapc25tfwkm3.run', 3)
	from_a_trec_run('ancapc100tfwkm.run', 1)
	from_a_trec_run('ancapc100tfwkm2.run', 2)
	from_a_trec_run('ancapc100tfwkm3.run', 3)
	
	from_a_trec_run('bnnbnn3tfwkm.run', 1)
	from_a_trec_run('bnnbnn3tfwkm2.run', 2)
	from_a_trec_run('bnnbnn3tfwkm3.run', 3)
	from_a_trec_run('bnnbnn25tfwkm.run', 1)
	from_a_trec_run('bnnbnn25tfwkm2.run', 2)
	from_a_trec_run('bnnbnn25tfwkm3.run', 3)
	from_a_trec_run('bnnbnn100tfwkm.run', 1)
	from_a_trec_run('bnnbnn100tfwkm2.run', 2)
	from_a_trec_run('bnnbnn100tfwkm3.run', 3)
	
	from_a_trec_run('lncltn3tfwkm.run', 1)
	from_a_trec_run('lncltn3tfwkm2.run', 2)
	from_a_trec_run('lncltn3tfwkm3.run', 3)
	from_a_trec_run('lncltn25tfwkm.run', 1)
	from_a_trec_run('lncltn25tfwkm2.run', 2)
	from_a_trec_run('lncltn25tfwkm3.run', 3)
	from_a_trec_run('lncltn100tfwkm.run', 1)
	from_a_trec_run('lncltn100tfwkm2.run', 2)
	from_a_trec_run('lncltn100tfwkm3.run', 3)
	
	from_a_trec_run('expanded1.run', 1)
	
	from_a_trec_run('expandedPCA1.run', 1)
	
	from_a_trec_run('tfpca1.run', 1)
	'''
	from_a_trec_run('pcacluster1.run', 1)
	
	from_a_trec_run('pcafeature1.run', 1)
	

