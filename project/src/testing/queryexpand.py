# this code is designed to commence query expansion with the pca features included in the documents
import pickle
import basic_clustering
import tfkm

#q' = lambdaQ0 + (1-lambda)Qrm


# expansion terms

# the following opens a .run file and looks at the top returned documents
def getRetrieved(filname):
	
	retforall = {}
	
	fil = open(filname)

	old_index = -1

	returned = []

	for line in fil:
		
		terms = line.split()
		
		if old_index == -1:
			old_index = terms[0]
			retforall[old_index] = []
		
		new_index = terms[0]
		if new_index == old_index:
			retforall[old_index].append(terms[2].replace("_", " "))
		else:
		
			old_index = new_index
			
			retforall[old_index] = []
			
			
			retforall[old_index].append(terms[2].replace("_", " "))
	
	return retforall

# given a list of every returned document for a query, this reduces it to only the top k of those documents
def justtopk(allret, k):
	
	nd = {}
	
	for qn in allret.keys():
		nd[qn] = []
		counter = 0
		for ret in allret[qn]:
			counter += 1
			if counter <= k:
				nd[qn].append(ret)
	
	return nd

# the following is a slow be effective way of finding the index of an episode (magic ID) given its formal ID
def epname2num(ep):
	episode_numbers = pickle.load(open("id.p", "rb"))
	

	
	
	return 	episode_numbers.index(ep)

# the following gets only the vectors of the desired episodes from a list of all episode vectors
def getslice(vecs, wants):
	slice = []
	episode_numbers = pickle.load(open("id.p", "rb"))
	for thing in wants:
		index = episode_numbers.index(thing)
		slice.append(vecs[index])
		
	return slice

# mnq stands for make new query, this takes in the old query, the expansion terms, and lambda and uses them to make a new query vector
def mnq(oldq, expand, lamb):
	
	
	theQvec = tfkm.tovecform(oldq)
	
	
	
	for term in theQvec.keys():
		theQvec[term] = lamb * theQvec[term]
	
	for term in expand:
		if term in theQvec:
			theQvec[term] = theQvec[term] + (1-lamb) * expand[term]
		else:
			theQvec[term] = (1-lamb) * expand[term]
	
	return theQvec
	
# the following runs query expansion on every combination of tf-idf similarities and query sets
def expandall():
	
	lamb = 0.5
	
	
	filnams = ['km1lncltnq1.run','km1bnnbnnq1.run','km1ancapcq1.run','km1lncltnq2.run','km1bnnbnnq2.run','km1ancapcq2.run','km1lncltnq3.run','km1bnnbnnq3.run','km1ancapcq3.run']
	
	outfilnams = ['qexp1lncltn.run', 'qexp1bnnbnn.run', 'qexp1ancapc.run','qexp2lncltn.run', 'qexp2bnnbnn.run', 'qexp2ancapc.run','qexp3lncltn.run', 'qexp3bnnbnn.run', 'qexp3ancapc.run']
	
	qlistlist = ['Qtex.p','Qtex.p','Qtex.p','Q2tex.p','Q2tex.p','Q2tex.p','Q3tex.p','Q3tex.p','Q3tex.p',]
	
	
	allvecs = pickle.load(open("centroid_helper.p", "rb"))
		
	
	
	for alpha in range(len(filnams)):
	
		fillnam = filnams[alpha]
		
		
		
		allret = getRetrieved(fillnam)
		
		nallret = justtopk(allret, 10)
		
		allq = pickle.load(open(qlistlist[alpha],'rb'))
		
		outf = outfilnams[alpha]
		
		ofil = open(outf, 'w')
		ofil.close()
		
		
		centroids = pickle.load(open('c20.p', 'rb'))
		
		# for every query in the query set, determine the terms to expand it by by taking the centroid of the top 10 returned values, then run that new query
		for qn in nallret.keys():
			slice = getslice(allvecs, nallret[qn])
			expandterms = basic_clustering.centroid_func(slice)
			
			newquery = mnq(allq[int(qn)], expandterms, lamb)
			
			#print(newquery)
			#print(allvecs[0])

			style = alpha % 3
			
			#if style == 0:
			#	print(newquery)
			#	print(allvecs[0])
			
			tfkm.handleQ2b(newquery, qn, outf, style, allvecs)
			
		
	
	return



if __name__ == '__main__':
	
	expandall()
	
# step 1: search on q0
# step 2: generateQrm based on search
# step 3: create q' based on qrm
# step 4: search on q'

