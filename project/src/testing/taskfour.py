# the following code is designed to run lucene's default bm25 analyzer

INDEX_DIR = "IndexFiles.index"

import sys, os, lucene

import pickle

import doPrint_alt

import India_tokenize

from java.nio.file import Paths
from org.apache.lucene.analysis.standard import StandardAnalyzer
from org.apache.lucene.index import DirectoryReader
from org.apache.lucene.queryparser.classic import QueryParser
from org.apache.lucene.store import SimpleFSDirectory
from org.apache.lucene.search import IndexSearcher

"""
This script is loosely based on the Lucene (java implementation) demo class
org.apache.lucene.demo.SearchFiles.  It will prompt for a search query, then it
will search the Lucene index in the current directory called 'index' for the
search query entered against the 'contents' field.  It will then display the
'path' and 'name' fields for each of the hits it finds in the index.  Note that
search.close() is currently commented out because it causes a stack overflow in
some cases.
"""
def run(searcher, analyzer):
	someint = 0
	while False:
		print()
		print("Hit enter with no input to quit.")

		someint = someint + 1

		if someint == 1:
			command = "power nap benefits"
		elif someint == 2:
			command = "whale vocalization production of sound"
		elif someint == 3:
			command = "pokemon puzzle league"
		else:
			command = input("Query:")


		if command == '':
			return

		print()
		print("Searching for:", command)
		query = QueryParser("contents", analyzer).parse(command)
		scoreDocs = searcher.search(query, 10).scoreDocs
		print("%s total matching documents." % len(scoreDocs))

		for scoreDoc in scoreDocs:
			doc = searcher.doc(scoreDoc.doc)
			print('paragraph ID:', doc.get("paragraph ID"), 'text:', doc.get("txt"))



def make_the_run(searcher, analyzer):
	
	
	theqlist = ['Qtex.p', 'Q2tex.p', 'Q3tex.p']
	therunlist = ['lucene.run', 'lucene2.run', 'lucene3.run']
	
	nonindex = -1
	
	for qtex in theqlist:
		nonindex += 1
		runfil = therunlist[nonindex]
	
	
	

		
		all_queries = pickle.load(open(qtex,"rb"))
		file = open(runfil, "w")
		file.close()
		
		stopword = India_tokenize.getstopword()
		
		qnum = 0;

		for query in all_queries:
			
			token_query = India_tokenize.tokenize_india(query, stopword)
			
			command = ""
			
			for word in token_query:
				command = command + str(word)
				command = command + " "
			
			
			list1 = []
			list2 = []
			
			#print("Searching for:", command)
			query = QueryParser("contents", analyzer).parse(command)
			scoreDocs = searcher.search(query, 10).scoreDocs
			#print("%s total matching documents." % len(scoreDocs))

			for scoreDoc in scoreDocs:
				doc = searcher.doc(scoreDoc.doc)
				
				list1.append(doc.get("paragraph ID"))
				
				list2.append(99)
				
				#print('paragraph ID:', doc.get("paragraph ID"), 'text:', doc.get("txt"))

			doPrint_alt.treccarPrint(list1, str(qnum), "lucene", runfil, list2)
			
			qnum = qnum + 1



if __name__ == '__main__':
	lucene.initVM(vmargs=['-Djava.awt.headless=true'])
	print('lucene', lucene.VERSION)
	base_dir = os.path.dirname(os.path.abspath(sys.argv[0]))
	directory = SimpleFSDirectory(Paths.get(os.path.join(base_dir, INDEX_DIR)))
	searcher = IndexSearcher(DirectoryReader.open(directory))
	analyzer = StandardAnalyzer()
	
	make_the_run(searcher, analyzer)
	
	run(searcher, analyzer)
	del searcher