import tfkm
import qexpandWpca


import pickle

# the following function is designed to use pca clustering as an additional feature to compute tf-idf


# the following is designed to handle every query
def computeQs():
	
	filnams = ['km1lncltn1.run','km1bnnbnn1.run','km1ancapc1.run','km1lncltn2.run','km1bnnbnn2.run','km1ancapc2.run','km1lncltn3.run','km1bnnbnn2.run','km1ancapc3.run']
	
	outfilnams = ['tfpca1lncltn.run', 'tfpca1bnnbnn.run', 'tfpca1ancapc.run','tfpca2lncltn.run', 'tfpca2bnnbnn.run', 'tfpca2ancapc.run','tfpca3lncltn.run', 'tfpca3bnnbnn.run', 'tfpca3ancapc.run']
	
	qlistlist = ['Qtex.p','Qtex.p','Qtex.p','Q2tex.p','Q2tex.p','Q2tex.p','Q3tex.p','Q3tex.p','Q3tex.p',]
	
	
	
	k = 100
	# first the pca features are loaded
	pcamat = qexpandWpca.loadupfeature(k)
	
	#print(pcamat)
	
	allvecs = pickle.load(open("centroid_helper.p", "rb"))
	
	allq = pickle.load(open('Qtex.p','rb'))
	# then the pca features are added to the vector representation
	vecs = qexpandWpca.PCAtoallVecs(allvecs, pcamat)
	#vecs = allvecs
	
	for alpha in range(len(filnams)):
		
		outf = outfilnams[alpha]
		ofil = open(outf, 'w')
		ofil.close()
		
		qlist = qlistlist[alpha]
		
		
		outstr = outfilnams[alpha]
		
		sind = alpha % 3
		
		queries = pickle.load(open(qlist, 'rb'))
		qnum = 0
		
		centroids = []
		
		for x in range(k):
			xdic = {}
			xdic[x] = 1
			centroids.append(xdic)
		
		# finally, for every query the query is handled by the handler, with each 'cluster' being defined by a feature
		# the handler will simply identify which features are most prominent amongst the K top returned documents and add those to the queries vector representation as features with various weights 
		for query in queries:
			
			qvec = tfkm.tovecform(query)
			
			tfkm.handleQ2(qvec, qnum, outstr, sind, centroids, allvecs)
			qnum += 1
			
		
		
		
		
	return True
	


if __name__ == '__main__':
	computeQs()



