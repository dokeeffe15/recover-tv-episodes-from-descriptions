import pickle
import do_tfidf
import random
import tfkm
import doPrint
import math


docfreq = pickle.load(open("df.p", "rb"))
cosnorm = pickle.load(open("cosn.p", "rb"))
id2 = pickle.load(open("idtoid.p", "rb"))
N = len(id2)

data = pickle.load(open("tex.p", "rb"))
episode_numbers = pickle.load(open("id.p", "rb"))
centroid_helper = pickle.load(open("centroid_helper.p", "rb"))
maxtermf = pickle.load(open("maxtf.p", "rb"))


# This is a function designed to take in documents passed in vector form and calculate the centroid of those documents, also in vector form
def centroid_func(centroid_helper_slice):

    dic = {}

# For every word in every document, its score is added to the centroid score, and then normalized at the end
    for doc in centroid_helper_slice:
        for [word, value] in doc.items():
            if word not in dic:
                 dic[word] = value
            else:
                dic[word] += value

    for [word, value] in dic.items():
        dic[word] = value / len(centroid_helper_slice)

    return dic


# This is a clustering algorith, designed to, given only the number k and the style of tf-idf similarity to use, create K different clusters of documents
def k_means(k, style):

    docfreq = pickle.load(open("df.p", "rb"))
    maxtermf = pickle.load(open("maxtf.p", "rb"))
    cosnorm = pickle.load(open("cosn.p", "rb"))
    id2 = pickle.load(open("idtoid.p", "rb"))
    N = len(id2)

    # ignore the stuff above, this stuff will probably be changed to pass to the function


    # this is like a list you can make the centroids of your clusters
    centroids = []

    inacluster = {}

# the original starting cluster centroids are chosen randomly
    for x in range(k):
        inacluster[x] = []
        centroids.append(centroid_helper[random.randint(0,len(centroid_helper)-1)])


    # while the centroids haven't stabilized

    old_centroids = []
    while(old_centroids != centroids):
        old_centroids = centroids
        index = -1

        #compute for every episode the centroid its closest too 


        for episode in centroid_helper:
            index = index + 1
            # score list
            scores = []

        # for each centroid
            for centroid in centroids:
                # score_list.append(tfidf score)
                score = do_tfidf.lncltn_score(centroid, episode, docfreq, N, cosnorm[index])
                if style == 1:
                    score = do_tfidf.bnnbnn_score(centroid, episode)
                elif style == 2:
                    cn2 = tfkm.cnorm(centroid)
                    score = do_tfidf.ancapc_score(centroid, episode, docfreq, N, cosnorm[index], cn2, maxtermf[index])
                scores.append(score)


            # put document in whichever cluster has the highest score


            smax = max(scores)
            clustnum = scores.index(smax)


            inacluster[clustnum].append(index)

            #recomputing centroids in each cluster

        for clust in inacluster.keys():
            slice = []
            for ep in inacluster[clust]:
                #centroid_helper[ep][clust] = 1
                slice.append(centroid_helper[ep])
            centroids[clust] = centroid_func(slice)




    return inacluster



# this function uses the existing clusters to, for a given query, identify which cluster is closest to it, then commencs the appropriate tf-idf scoring on only that cluster
def k_means_retrieval(query, clusters, cluster_centroid, fil, style, qnum):

    theQvec = tfkm.tovecform(query)
    closeClusters = tfkm.getKcc_lncltn(theQvec, cluster_centroid, 1)
    #print(query)
    #print(closeClusters)


    retrieval_list = {}
    for document in clusters[closeClusters[0]]:
        #print(episode_numbers[document])
        score = do_tfidf.lncltn_score(theQvec, centroid_helper[document], docfreq, N, cosnorm[document])
        if style == 1:
            score = do_tfidf.bnnbnn_score(theQvec, centroid_helper[document])
        elif style == 2:
            cn2 = tfkm.cnorm(theQvec)
            score = do_tfidf.ancapc_score(theQvec, centroid_helper[document], docfreq, N, cosnorm[document], cn2, maxtermf[document])
        retrieval_list[document + 1] = score
    
    retrieval_list = sorted(retrieval_list.items(), key=lambda x: x[1] * -1)

    index = 0
    list1 = []
    list2 = []
    #print(scoredic)
    for values in retrieval_list:
        index +=1
        if ((index <= 100)):
            list1.append(values[0])
            list2.append(values[1])




    # print those top 100 items added to the list in trec format


    doPrint.treccarPrint(list1, str(qnum), "km", fil, list2)


    return retrieval_list


# centroid_helper would more accurately be the episodes in vector notation

# centroid_helper is a list of dictionaries, 1 dictionary per episode, with words and frequencies in the dictionairy
# centroid_helper = [dict_ep1, dict_ep2, dict_ep3.....]
# dict_ep1 = {word1: tf, word2: tf, ...}


# this is a method to convert a document from a tf vector back to a list of terms 
def converter(doc):

    list = []
    for [word, value] in doc.items():
        list.append(word)

    return list


if __name__ == '__main__':

    qlistlist = ['Qtex.p',  'Q2tex.p', 'Q3tex.p']
    outlist = ['q1.run', 'q2.run', 'q3.run']
    #klist = [1,2,3,4,5,10,15,20,25,100]
    klist = [1,2,3,20]
    styles = ['lncltn', 'bnnbnn', 'ancapc']


# for all above combinations of cluster numbers, styles of tf-idf similarity, and query sets, run the k means retrieval on every query
    for theK in klist:
        indi = -1
        for qlist in qlistlist:
            indi += 1
            
            innerindex = -1
            for thestyle in styles:
                innerindex += 1
# The clusters are computed here
                clusts = k_means(theK, innerindex)
                outfil = 'km' + str(theK) + styles[innerindex] + outlist[indi]
                ofil = open(outfil, 'w')
                ofil.close()
                queries = pickle.load(open(qlist, 'rb'))
                qnum = 0
# because only the clusters are returned, and not the centroids, the centroids need to be recomputed
                centroids = []
                for z in range(theK):
                    centroids.append(0)
    
                for clust in clusts.keys():
                    slice = []
                    for ep in clusts[clust]:
                        #centroid_helper[ep][clust] = 1
                        slice.append(centroid_helper[ep])
                    centroids[clust] = centroid_func(slice)

                ns = 'c' + str(theK) + '.p'
                pickle.dump(centroids, open(ns, 'wb'))
# once all the centroids and clusters have been computered, on every query the k_mean_retrieval can be run
                for query in queries:
                    k_means_retrieval(query, clusts, centroids, outfil, innerindex, qnum)
                    qnum += 1

    #print(int(math.sqrt(len(episode_numbers))))


'''
    theK = 1



    clusts = k_means(theK, 3)

    print(clusts)

    for thing in clusts[0]:
        print(episode_numbers[thing])

    for cluster in clusts:
        print(clusts[cluster])

    centroids = []
    for z in range(theK):
        centroids.append(0)
    
    for clust in clusts.keys():
        slice = []
        for ep in clusts[clust]:
            #centroid_helper[ep][clust] = 1
            slice.append(centroid_helper[ep])
        centroids[clust] = centroid_func(slice)

    #handleQ('Constable Odo, Gamma Quadrant, Deep Space Nine')

    qlistlist = ['Qtex.p',  'Q3tex.p', 'Q2tex.p']
    outlist = ['km.run', 'km3.run', 'km2.run']
    index = -1

    for qlist in qlistlist:
        index += 1
        outfil = open(outlist[index], 'w')
        outfil.close()

        queries = pickle.load(open(qlist, 'rb'))

        qnum = 0
        for query in queries:
            k_means_retrieval(query, clusts, centroids, outlist[index], 3)
            qnum += 1

    #print(k_means_retrieval("joey and ross had a fight over who gets custody of the baby", clusts, centroids))
''' 

