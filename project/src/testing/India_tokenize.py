# This code is designed to be imported to allow the team India tokenizer to be used without recopying the code every time
import Stemmer

# This function takes in strtosplit: string to split, and a list of stopwords

def tokenize_india(strtosplit, stopword):
	Qwords = []
	word = ""
	cw = 0
	stemmer = Stemmer.Stemmer('english')
	
	for letter in strtosplit:
		# First all non letter symbols are removed from the words in the string and tokenized on
		# Capital letters are lowercased
		if (ord(letter) >= 65) & (ord(letter) <= 90):
			# print("cap")
			letter = chr(ord(letter) + 32)
			word = word + letter
		elif (((ord(letter) >= 97) & (ord(letter) <= 122)) or (ord(letter)<=57 & ord(letter)>=48)):
			# print("lower")
			word = word + letter
		else:
			# words less than a length of 3 are removed
			if(ord(letter) != 46):
				
				if (len(word) <= 2):
					word = ""
				else:
					word = stemmer.stemWord(word)
					if (len(word) <= 2):
						word = ""
					else:
						# the words are stemmed, and if still longer than 2 added to a list
						cw = cw + 1
						Qwords.append(word)
						# Qwords is a list of the tokenized and stemmed words
					word = ""
	if (len(word) > 2):
		word = stemmer.stemWord(word)
		if (len(word) > 2):
			Qwords.append(word)
	for text in stopword:
			while ((len(stemmer.stemWord(text)) > 2) & (stemmer.stemWord(text) in Qwords)):
				Qwords.remove(stemmer.stemWord(text))

	Qwords.sort()
	# All of the stor words are removed and the list is sorted before being returned.
	return Qwords
	
	


#	Qwords = list(set(Qwords))


def getstopword():

	stopword = []
	thefile = open("Jstopword.txt", "r")
	for x in thefile:
		stopword.append(x.strip())
	thefile.close()
	stemmer = Stemmer.Stemmer('english')
	
	return stopword
		


#stopword = getstopword()

#str1 = "The Cookie cookie cookiE monster is was and you are is was cool"

#print(tokenize_india(str1, stopword))



	