import numpy as np
import do_tfidf
import tfkm
import pickle

# the following code is designed to load in a massive matrix of feature vectors. the matrix is currenlty saved as an array, which will then have pca computed on it later
# that array is stored in such a way to prevent this script from being run more than necesarry. 

id = pickle.load(open("id.p", "rb"))
tex = pickle.load(open("tex.p", "rb"))
Qid = pickle.load(open("Qid.p", "rb"))
Qtex = pickle.load(open("Qtex.p", "rb"))
docfreq = pickle.load(open("df.p", "rb"))


gamma = 0.5
score = 0
array = []

for x in range(0,len(tex)):
    for y in range(0,len(Qtex)):
        temp1 = tfkm.tovecform(tex[x])
        temp2 = tfkm.tovecform(Qtex[y])
        if (x==y):
            score = 14 * gamma + (1 - gamma) * do_tfidf.lncltn_score(temp1,temp2,docfreq,len(tex),tfkm.cnorm(temp1))
            array.append(score)
            print('PCA being made. 1 time index time operation. Percent done:')
            print((x / len(tex))*100)
        else:
            score = (1 - gamma) * do_tfidf.lncltn_score(temp1,temp2,docfreq,len(tex),tfkm.cnorm(temp1))
            array.append(score)

pickle.dump(array, open("array.p", "wb"))
