import pickle

# designed to print things in trec run format
# rlist is a list of returned documents
# qn is the query name / query ID to print with each returned document in the list
# whichbool is the name of the method
# fil is the name of the file to print to
# score is a list of scores. each index in score corresponds to an index in rlist.
def treccarPrint(rlist, qn, whichbool, fil, score):

# this keybreaker converts from internal doc ID to paragraph ID
    keybreaker = pickle.load(open("idtoid.p", "rb"))

    thef = open(fil, "a")
    z=0

# print all iterms in the provided list to a file in trec car run format
    for magicID in rlist:
        z = z + 1
        thef.write(qn)
        thef.write(" ")
        thef.write("Q0")

        thef.write(" ")
        mstring = keybreaker[int(magicID) - 1].replace(" ", "_")
        thef.write(mstring)

        thef.write(" ")
        thef.write(str(z))
        thef.write(" ")
        thef.write(str(score[z-1]))
        thef.write(" ")
        thef.write("India-")
        thef.write(whichbool)
        thef.write("\n")



    thef.close()


