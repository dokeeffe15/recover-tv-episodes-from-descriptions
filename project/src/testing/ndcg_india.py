
# Created by Justin Leach


# This is now NDCG

import math

# ndcg = dcg(q) / idcg(q)

# dcg(q) = sum(2^grade(m) - 1) / (log2(1+m))

# idcg = dcg of ideal ranking

import sys

# The following reads a qrel file. this code is not called in the project. 
def readqrel():
	biglist =[]
	thebool = True
	print("opening qrel")
	fil = open("train.pages.cbor-article.qrels", "rb")
	for line in fil:
		word = ""
		thebool = True
		#print(line)
		#print(str(line))

			
		
		for onechar in str(line):
			#print(str(onechar))
	
			if ((onechar != " ") & (onechar != "\n")): 
				word = word + str(onechar)
				#print(word)
			else:
				if thebool is True:
					
					biglist.append(word)
					word = ""
					thebool = False
				else:
					word = ""
					thebool = True

		#print(line)
	fil.close()
	return biglist
	
	
	


#justwhatsneeded = readqrel()

#print(justwhatsneeded)

# the following reads a run file, from it taking in the retrieved documents for every query
def readrun(fname):

	biglist =[]
	thebool = True
	print("opening file")
	fil = open(fname, "rb")
	for line in fil:
		word = ""
		thebool = True
		#print(line)
		#print(str(line))

			
		
		for onechar in str(line):
			#print(str(onechar))
	
			if ((onechar != " ") & (onechar != "\n")): 
				word = word + str(onechar)
				#print(word)
			else:
				if ((thebool is True) & (len(word) != 3)):
					
					biglist.append(word)
					word = ""
					thebool = False
				elif len(word) == 3:
					word = ""
					thebool = False
				else:
					word = ""
					thebool = True

		#print(line)
	fil.close()
	return biglist
	

#thefirstrun = readrun("../otherRunFiles/booleanAND.run")
#print(thefirstrun)







# this is the grading function for super relevant documents. if a document is super relevant than it scores a grade of 2 instead of 1
def sgrade(x, supers):
	val = 1
	if x in supers:
		val += 1
	
	return val
# this is boolean. It either is relevant or it is not. There are no grades of elevance


# this is the precision at r function. 
def patr(relevant, present):
	k = len(relevant)
	val = patk(relevant, present, k)
	
	return val

# this is the precision at k function, it counts the number of relevant documents present before index k and divides by k
def patk(relevant, present, k):
	ind = 0
	
	prerec = 0
	
	
	
	for thing in present:
		if ind < k:
			if thing in relevant:
				prerec +=1
			
		ind += 1
	
	divd = prerec / k
	
	return divd




# dcg(q) = sum(2^grade(m) - 1) / (log2(1+m))


def sdcg(relevant, present, supers):
	thesum = 0
	for item in relevant:
		xitem = item.replace(" ", "_")
		if item in present:
			ind = present.index(item) + 1
			thesum = thesum + (pow(2, sgrade(item, supers)) - 1)/(math.log(1+ind)/math.log(2))
			
	return thesum
	
	
# idcg = dcg of ideal ranking

def sidcg(relevant, supers):
	thesum = 0
	
	ind = 1
	
	for item in supers:
		thesum = thesum + (pow(2, sgrade(item, supers)) - 1)/(math.log(1+ind)/math.log(2))
		ind += 1
	
	for item in relevant:
		if item not in supers:
			thesum = thesum + (pow(2, sgrade(item, supers)) - 1)/(math.log(1+ind)/math.log(2))
			ind += 1
	return thesum
		
# the sndcg is the same as the ndcg function, except for grading it calls the grade function which grades super relevant documents better
def sndcg(relevant, present, supers):
	thedcg = sdcg(relevant, present, supers)
	
	if thedcg <= 0.00001:
		return 0
	
	nr = []
	

	for item in relevant:
		nr.append(item)
	
	
	ideal = sidcg(nr, supers)
	answr= thedcg / ideal
	return answr












def grade(x):
	return 1
# this is boolean. It either is relevant or it is not. There are no grades of elevance






# dcg(q) = sum(2^grade(m) - 1) / (log2(1+m))


def dcg(relevant, present):
	thesum = 0
	for item in relevant:
		xitem = item.replace(" ", "_")
		if item in present:
			ind = present.index(item) + 1
			thesum = thesum + (pow(2, grade(item)) - 1)/(math.log(1+ind)/math.log(2))
			
	return thesum
	
	
# idcg = dcg of ideal ranking

def idcg(relevant):
	thesum = 0
	for item in relevant:
		ind = relevant.index(item) + 1
		thesum = thesum + (pow(2, grade(item)) - 1)/(math.log(1+ind)/math.log(2))
	return thesum
		

def ndcg(relevant, present):
	thedcg = dcg(relevant, present)
	
	if thedcg <= 0.00001:
		return 0
	
	nr = []
	
	if len(relevant) > 20:
		for xi in range(20):
			#print(xi)
			nr.append(relevant[xi])
	else:
		for item in relevant:
			nr.append(item)
		
	
	ideal = idcg(nr)
	answr= thedcg / ideal
	return answr


# this code was designed for trec-car tools purposes and was not used in the project
def averagendcgTWENTY(toopen):
	#print("Please type the path to and name of the .run file to open")
	#toopen = input()
	
	print("opening")
	print(toopen)
	
	fullrel = readqrel()
	fullpres = readrun(toopen)
	
	ndcgscores = []
	
	ji = 0
	ii = 0
	redalert = False
	
	while (ji < len(fullrel)):
		
		if redalert == False:
			idstr = str(fullrel[ji])
		else:
			while idstr == str(fullrel[ji]):
				ji = ji + 2
			idstr = str(fullrel[ji])
			
			
		relevant = []
		present = []
		mr = True
		redalert = False
		while mr:
			relevant.append(str(fullrel[ji+1]))
			ji = ji + 2
			if ji >= len(fullrel):
				mr = False
			elif str(fullrel[ji]) != idstr:
				mr = False
			#elif len(relevant) >= 20:
				#mr = False
				#redalert = True
		
		mp = False
		for item in fullpres:
			if ((str(item) == idstr) & (len(present) < 20)):
				mp = True
			elif mp == True:
				mp = False
				present.append(str(item))
				
				
		"""
		mp = True
		
		if (ii >= len(fullpres)):
			mp = False
			print("len failr")
		elif str(fullpres[ii]) != idstr:
			mp = False
			print("equal fail")
			print(str(fullpres[ii]))
			print(idstr)
		
		while mp:
			present.append(str(fullpres[ii+1]))
			
			print(ii)
			print(ji)
			
			ii = ii+2
			if (ii >= len(fullpres)):
				mp = False
			elif str(fullpres[ii] != idstr):
				mp = False
		
		
		#print(relevant)
		#print(present)
		
		"""
		
		QsNDCG = ndcg(relevant, present)
		ndcgscores.append(QsNDCG)
	print(ndcgscores)
	av = sum(ndcgscores)/len(ndcgscores)
	print(av)
	
	
	
if __name__ == '__main__':
	print("doing main part")
	toopen = sys.argv[1]
	averagendcgTWENTY(toopen)

