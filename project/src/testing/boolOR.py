import pickle

#posting_list = pickle.load(open("posting.p", "rb"))

# same merge algorithm as merge in Merge_Algorithm.py
# only difference is list is appended if either list is not empty
# double for loops at the end to ensure no doc ID is left in either list
def mergeor(posting_listword1,posting_listword2):

   i = 0
   j = 0

   new_list = []

   while i < len(posting_listword1) and j < len(posting_listword2):
      if posting_listword1[i] == posting_listword2[j]:
         new_list.append(posting_listword1[i])
         i+=1
         j+=1
      elif posting_listword1[i] < posting_listword2[j]:
         new_list.append(posting_listword1[i])
         i+=1
      else:
         new_list.append(posting_listword2[j])
         j+=1

   while i < len(posting_listword1):
      new_list.append(posting_listword1[i])
      i += 1
   while j < len(posting_listword2):
      new_list.append(posting_listword2[j])
      j += 1
       
   return new_list


#print(mergeor(posting_list["censorship"],posting_list["bbc"]))

