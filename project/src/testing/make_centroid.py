
import pickle
import Stemmer
import India_tokenize


#The following is the confusingly named make_centroid. It does not actually make a centroid.
# for the code that makes a centroid see basic_clustering.py
# this code instead takes every single document, converts them into vector notation, and saves them using pickle
# prior to the implementation of this code, vector notation was more abstract and not literally used
# this code was created to assist the centroid based functions, hence the confusing name.


def convert_tovec():

	index = 0
	data = pickle.load(open("tex.p", "rb"))
	episode_numbers = pickle.load(open("id.p", "rb"))


	
	stemmer = Stemmer.Stemmer('english')
	stopword = []
	stopword = India_tokenize.getstopword()

	document_list = []
# every word in every episode is simply expressed in vector notation through a dictionairy of term to term frequency
	for episode in data:
		episode # is a string that contains a summary of an episode of friends
		episode_numbers[index] # is a string that works as the episode id, in the format season.episode
		index = index + 1

		tokenlist = India_tokenize.tokenize_india(episode, stopword)

		dic = {}

		for token in tokenlist:
			if token not in dic:
				 dic[token] = 1
			else:
				dic[token] += 1

		document_list.append(dic)


	pickle.dump(document_list,(open("centroid_helper.p","wb")))

if __name__ == '__main__':
	convert_tovec()
