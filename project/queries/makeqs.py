import pickle

import random

from datetime import datetime

# THE FOLLOWING MAKES THE QUERIES

fil = open('forthequeries.txt', 'r')

ln = -1

IDs = {}
fulltext = []
comparativetxt = ""

for lin in fil:
	ln = ln + 1
	
	words = lin.split()
	IDs[ln] = []
	
	#print(words)
	# if the line doesnt have a string that can be a query forget about it
	if len(words) > 2:
		comparativetxt = ""
		IDs[ln].append(str(words[0]).strip().strip('\n'))
	else:
		# otherwise the first thing is the episode number and should be stored as relevant
		IDs[ln].append(IDs[ln - 1][0])


	
	for w in words:
			#print(w)
		#	print(str(w))
		# all the other words are added to the string of the query
		if w != words[0]:
			comparativetxt =  str(comparativetxt) + " " + str(w)
	fulltext.append(comparativetxt)


fil.close()


ds9list = []

fil = open('ds9eps.txt')

for lin in fil:
	splitup = lin.split()
	epstr = ""
	flag = True
	for thing in splitup:
		if ((ord(thing[0]) <= ord('9')) & (ord(thing[0]) >= ord('0'))):
			flag = False
		if flag:
			epstr = epstr + ' ' + thing
	ds9list.append(epstr.strip().strip('\n'))
	
fil.close()


'''

fil = open('deepspace.txt')
zeroT = ds9list[0]
firstT = ds9list[0]
secondT = ds9list[1]

#print(ds9list)

index = 1

thestr = ""
lnum = 0

target = 0

for lin in fil:
	
	firstT = firstT.strip()
	secondT = secondT.strip()
	zeroT = zeroT.strip()
	
	lnum += 1
	if lnum > target:
		
		#print(lin)
		#print(firstT)
		
		if str(firstT).lower() in str(lin).lower():
			target = lnum + 3
			zeroT = firstT
			firstT = secondT
			index += 1
			if index < len(ds9list):
				secondT = ds9list[index]
			
		elif str(secondT) in str(lin):
			target = lnum + 3
			print(firstT)
			zeroT = secondT
			
			index += 1
			if index < len(ds9list):
				firstT = ds9list[index]
			index += 1
			if index < len(ds9list):
				secondT = ds9list[index]
			
			
	elif lnum == target:
		ln += 1
		IDs[ln] = []
		IDs[ln].append(str(zeroT))
		
		fulltext.append(lin)
	
			
	

#print(IDs)

#print(fulltext)

'''






fil = open('ds9.txt')

zeroT = ds9list[0]
firstT = ds9list[1]
secondT = ds9list[2]

#print(ds9list)

index = 2

thestr = ""



for lin in fil:

	
	if firstT in lin:
		ln+=1
		IDs[ln] = []
		IDs[ln].append(zeroT.strip().strip('\n'))
		fulltext.append(thestr)
		
		
		
		thestr = ""
		thestr = lin
		
		index += 1
		
		zeroT = firstT
		firstT = secondT
		
		if index < len(ds9list):
			secondT = ds9list[index]
		
		
	elif secondT in lin:
		print(firstT)
		ln+=1
		IDs[ln] = []
		IDs[ln].append(zeroT.strip().strip('\n'))
		fulltext.append(thestr)
		
		
		
		thestr = ""
		thestr = lin
		
		index += 1
		
		zeroT = secondT
		
		
		if index < len(ds9list):
			firstT = ds9list[index]
		index += 1
		if index < len(ds9list):
			secondT = ds9list[index]
		
		
	else:
		thestr = lin
			
ln+=1
IDs[ln] = []
IDs[ln].append(zeroT.strip().strip('\n'))
fulltext.append(thestr)
thestr = ""

















pickle.dump(IDs, open("Qid.p", 'wb'))
pickle.dump(fulltext, open("Qtex.p", 'wb'))








phil = open('q2set.txt', 'r')

word = ''

nextw = True

queries = {}
qtext = []
sidelist = []



index = 0
for lin in phil:
	
	if len(lin) < 2:
		nextw = True
		
		qtext.append(word)
		
		queries[index] = []
		
		for epi in sidelist:
			
			if ((epi.strip().strip('\n') in ds9list)):
				queries[index].append(epi.strip().strip('\n'))
			else:
				print(epi)
		
		index += 1
		sidelist = []
		
		
	elif nextw:
		word = lin
		nextw = False
	else:
		sidelist.append(lin)
	
	
phil.close()

pickle.dump(queries, open("Q2id.p", 'wb'))
pickle.dump(qtext, open("Q2tex.p", 'wb'))

print(queries)

#print(qtext)

#print(ds9list)









# SCRAMBLER QUERIES

# number of each type of query
z = 5





qin1 = len(fulltext)
qin2 = len(qtext)

listQ = []
dicQ = {}
superrels = {}
index = -1


random.seed(datetime.now())



for maxw in [10, 20]:
	for fq1 in [0, 1, 2]:
		for fq2 in [1, 2, 5]:
			for qr in range(z):
				
				index = index + 1
				
				comparativetxt = ""
				
				dicQ[index] = []
				superrels[index] = []
				
				for q2s in range(fq2):
					theint = random.randint(0, qin2-1)
					comparativetxt = comparativetxt + ' ' + qtext[theint]
					
					for rel in queries[theint]:
						if rel not in dicQ[index]:
							dicQ[index].append(rel.strip().strip('\n'))
						else:
							superrels[index].append(rel.strip().strip('\n'))
					
					
				
				for q1s in range(fq1):
					theint = random.randint(0, qin1-1)
					longstring = fulltext[theint]
					longstringlist = longstring.split()
					
					j = 0
					
					for shortword in longstringlist:
						j += 1
						if j <= maxw:
							comparativetxt = comparativetxt + ' ' + shortword
					
					
					#print(IDs[theint])
					
					for rel in IDs[theint]:
						if rel not in dicQ[index]:
							dicQ[index].append(rel.strip().strip('\n'))
						else:
							superrels[index].append(rel.strip().strip('\n'))
							
				listQ.append(comparativetxt)
					

#print(dicQ)
#print(listQ)
#print(superrels)

pickle.dump(dicQ, open("Q3id.p", 'wb'))
pickle.dump(listQ, open("Q3tex.p", 'wb'))
pickle.dump(superrels, open("Q3super.p", 'wb'))






