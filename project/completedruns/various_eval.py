
import ndcg_india

import pickle

import std_err

import os

def from_a_trec_run(filname, style):

	fil = open(filname)

	superflag = False
	prflag = False

	if style == 1:
		rel_docs = pickle.load(open("Qid.p","rb"))
	elif style == 2:
		rel_docs = pickle.load(open("Q2id.p","rb"))
		prflag = True 
	else:
		rel_docs = pickle.load(open("Q3id.p","rb"))
		sup_rel_docs = pickle.load(open("Q3super.p","rb"))
		superflag = True
		prflag = True



	old_index = -1

	ndcg_scores = []
	
	patr_scores = []

	returned = []

	for line in fil:
		
		
		
		
		terms = line.split()
		
		if old_index == -1:
			old_index = terms[0]
		
		new_index = terms[0]
		
		if int(new_index) >= len(rel_docs):
			if style == 2:
				style = 3
				print("STYLE CHANGE TO 3")
				return from_a_trec_run(filname, style)
			if style == 3:
				style = 1
				print("STYLE CHANGE TO 1")
				return from_a_trec_run(filname, style)
		
		
		if new_index == old_index:
			returned.append(terms[2].replace("_", " "))
		else:
			
			if len(rel_docs[int(old_index)]) < 1:
				print('\n *************** ')
				print(int(old_index))
			
			#print(returned)
			#print(rel_docs[int(old_index)])
			
			if superflag:
				score = ndcg_india.sndcg(rel_docs[int(old_index)], returned, sup_rel_docs[int(old_index)])
		
			else:
				score = ndcg_india.ndcg(rel_docs[int(old_index)], returned)
		
			ndcg_scores.append(score)
			
			if prflag:
				pscore = ndcg_india.patr(rel_docs[int(old_index)], returned)
				patr_scores.append(pscore)
			
		
			old_index = new_index
			
			returned = []
			returned.append(terms[2].replace("_", " "))
			
		
	completed = len(ndcg_scores)
	
	
		
	#print(ndcg_scores)
	# print the ndcg score and print their average. 
	
	while len(ndcg_scores) < len(rel_docs.keys()):
		ndcg_scores.append(0)
	
	while len(patr_scores) < len(rel_docs.keys()):
		patr_scores.append(0)
	
	av = sum(ndcg_scores)/len(ndcg_scores)
	print("From the run file:")
	print(filname)
	print("average of NDCG scores for each query of this retrieval")
	
	ndcg_scores.append(av)
	
	stder = std_err.stderr_func(ndcg_scores)
	
	print(av)
	print(av, '+/- ', stder)

	print("total queries run:")
	print(completed)
	
	if prflag:
		print('\n P at R scores:')
		#print(patr_scores)
		print('average: ')
		av = sum(patr_scores)/len(patr_scores)
		
		patr_scores.append(av)
	
		stder = std_err.stderr_func(patr_scores)
		
		print(av)
		print(av, '+/- ', stder)
	
	print('\n ---------------------------------------')

	fil.close()



if __name__ == '__main__':




	for filename in os.listdir("./"):
		if filename.endswith(".run"):
			if ((('2' in filename) and ('25' not in filename)) or ('2.run' in filename)):
				from_a_trec_run(filename, 2)
				print('^ in Q set 2')
			elif ((('3' in filename) and ('3tfw' not in filename)) or ('3.run' in filename)):
				from_a_trec_run(filename, 3)
				print('^ in Q set 3')
			else:
				from_a_trec_run(filename, 1)
				print('^ in Q set 1')


	'''
		from_a_trec_run('ancapc.run', 1)
		
		from_a_trec_run('lncltn.run', 1)
		
		from_a_trec_run('bnnbnn.run', 1)
		
		from_a_trec_run('or1.run', 1)
		
		from_a_trec_run('and1.run', 1)
		
		
		
		
		
		from_a_trec_run('ancapc2.run', 2)
		
		from_a_trec_run('lncltn2.run', 2)
		
		from_a_trec_run('bnnbnn2.run', 2)
		
		from_a_trec_run('or2.run', 2)
		
		from_a_trec_run('and2.run', 2)


		from_a_trec_run('ancapc3.run', 3)
		
		from_a_trec_run('lncltn3.run', 3)
		
		from_a_trec_run('bnnbnn3.run', 3)
		
		from_a_trec_run('or3.run', 3)
		
		from_a_trec_run('and3.run', 3)
		
		
		
		from_a_trec_run('lucene.run', 1)
		
		from_a_trec_run('lucene2.run', 2)
		
		from_a_trec_run('lucene3.run', 3)
		
	
		outlist = ['q1.run', 'q2.run', 'q3.run']
		klists = [1,2,3,4,5,10,15,20,25,100]
		styles = ['lncltn', 'bnnbnn', 'ancapc']
		
		for thek in klists:
			stind = -1
			for thestyles in styles:
				stind += 1
				qset = 0
				for o in outlist:
					qset += 1
					infil = 'km' + str(thek) + styles[stind] + o
					print(infil)
					#print(stind)
					if qset == 3:
						from_a_trec_run(infil, qset)


	'''

		
		