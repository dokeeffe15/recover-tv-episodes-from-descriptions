# This file is designed to calculate the standard error of values in a txt file
# This file makes the assumption the given text file has 3 elements per line, and the third element is the element to be used in calculations

import math
# The file can be changed from example.txt to any file to open 
def stderr_func(vals):
	
	# lv is the length of the values
	lv = len(vals)

	# This makes the assumption the last value in the document is the average
	average = vals[lv - 1]

	#av = sum(vals) / len(vals)

	# the standard deviation is the sum of the differences between the average, square rooted, then divided by the number of values
	stdev = 0
	for v in vals:
		stdev += pow(average - v, 2) 

	stdev = math.sqrt(stdev / lv)

	# standard error is standard deviation / sqrt(n)

	stderr = stdev / math.sqrt(lv - 1)

	return stderr



	


